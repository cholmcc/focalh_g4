//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Fast.hh
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Fast optical photon simulation
 */
#ifndef __Fast__
#define __Fast__
#include "G4VFastSimulationModel.hh"
class G4Region;
class G4OpBoundaryProcess;
class G4OpAbsorption;
class G4OpWLS;

/** Fast simulation model of optical photons in fibres.  See also

    - https://indico.cern.ch/event/915715/contributions/3850280/attachments/2036905/3410659/200512_shKo.pdf
    - https://github.com/HEP-FCC/dual-readout
*/
class Fast : public G4VFastSimulationModel
{
public:
  /** Constructor - need region? */
  Fast(G4Region* region)
    : G4VFastSimulationModel("Fast",region),
      fKill        (false),
      fSafety      (2),
      fAxis        (0),
      fTransportZ  (0),
      fNTransport  (0),
      fTransported (false),
      fGotProcess  (false),
      fBoundary    (0),
      fAbsorption  (0),
      fWLS         (0),
      fCurrent     (),
      fPrevious    ()
  {
    Reset();
  }
  ~Fast() {}

  /** Fast simulation model interface.

      Check if this should be applied to a particle type

      @param def Particle definition to check

      @return true if applicable to definition (optical photons) */
  virtual G4bool IsApplicable(const G4ParticleDefinition& def);
  /** Fast simulation model interface.

      Trigger of this fast model

      @param track

      @param true if this module should take care of the track */
  virtual G4bool ModelTrigger(const G4FastTrack& track);
  /** Fast simulation model interface.

      Do the actual step.

      @param track  Track
      @param step   Step */
  virtual void DoIt(const G4FastTrack& track, G4FastStep& step);

  /** Data structure to hold history
   */
  struct Data
  {
  public:
    G4int         fTrackNo       = -1;
    G4double      fKineticEnergy = 0;
    G4double      fGlobalTime    = 0;
    G4double      fPathLength    = 0;
    G4ThreeVector fPosition      = G4ThreeVector(0);
    G4ThreeVector fMomentum      = G4ThreeVector(0);
    G4ThreeVector fPolarization  = G4ThreeVector(0);
    G4int         fBoundary      = 0;
    G4double      fAbsorption    = std::numeric_limits<double>::max();
    G4double      fWLS           = std::numeric_limits<double>::max();
    G4double      fStepLength    = 0;
    /** Reset values */
    void Reset()
    {
      fTrackNo       = -1;		      
      fKineticEnergy = 0;		      
      fGlobalTime    = 0;		      
      fPathLength    = 0;		      
      fPosition      = G4ThreeVector(0);  
      fMomentum      = G4ThreeVector(0);  
      fPolarization  = G4ThreeVector(0);  
      fBoundary      = 0;		      
      fAbsorption    = 0;		      
      fWLS           = 0;                 
      fStepLength    = 0;
    }
    /** Assign from other */
    Data& operator=(const Data& other)
    {
      if (this == &other) return *this;
      fTrackNo       = other.fTrackNo;		      
      fKineticEnergy = other.fKineticEnergy;		      
      fGlobalTime    = other.fGlobalTime;		      
      fPathLength    = other.fPathLength;		      
      fPosition      = other.fPosition;  
      fMomentum      = other.fMomentum;  
      fPolarization  = other.fPolarization;  
      fBoundary      = other.fBoundary;		      
      fAbsorption    = other.fAbsorption;		      
      fWLS           = other.fWLS;
      fStepLength    = other.fStepLength;
      return *this;
    }
    /** Assign from other */
    Data& operator=(const G4Track& track)
    {
      fTrackNo       = track.GetTrackID();		      
      fKineticEnergy = track.GetKineticEnergy();		      
      fGlobalTime    = track.GetGlobalTime();		      
      fPathLength    = track.GetStepLength();		      
      fPosition      = track.GetPosition();  
      fMomentum      = track.GetMomentumDirection();  
      fPolarization  = track.GetPolarization();  
      return *this;
    }
    /** Compare to other */
    G4bool IsRepetitive(const Data& other, bool alsoStep=true) const;
    /** Add step length */
    Data& operator+=(G4double step)
    {
      fStepLength += step;
      return *this;
    }
  };
protected:
  /** Reset internal stuff */
  void Reset();
  /** Set step processes */
  void SetPostStepProcesses(const G4Track* track);
  /** Check for Next Interaction Length Left (NILL) */
  G4bool Check() const;
  /** Check for absorption */
  G4bool CheckAbsorption(G4double prev, G4double current) const;
  G4bool CheckTotalInternalReflection(const G4Track* track);

  G4bool               fKill;
  G4double             fSafety;
  G4ThreeVector        fAxis;
  G4double             fTransportZ;
  G4double             fNTransport;
  G4bool               fTransported;
  G4bool               fGotProcess;
  G4OpBoundaryProcess* fBoundary;
  G4OpAbsorption*      fAbsorption;
  G4OpWLS*             fWLS;
  Data                 fCurrent;
  Data                 fPrevious;
};

#endif
//
// EOF
//

