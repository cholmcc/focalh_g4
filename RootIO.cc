//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    RootIO.cc
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Singleton wrapper around ROOT I/O
 */
#include "RootIO.hh"
#include <G4SystemOfUnits.hh>

//--------------------------------------------------------------------
RootIO* RootIO::fgInstance = 0;

//--------------------------------------------------------------------
RootIO*
RootIO::Instance()
{
  if (!fgInstance) fgInstance = new RootIO();
  return fgInstance;
}

//--------------------------------------------------------------------
RootIO::RootIO()
  : fTree()
{
}

//--------------------------------------------------------------------
void
RootIO::AddFocalHSum(unsigned int copy,
		     double       edep,
		     double       light)
{
  fTree.AddFocalHSum(copy, edep  / GeV, light / GeV);
}

//--------------------------------------------------------------------
void
RootIO::AddFocalHPhoton(unsigned int copy,
			double       ekin)
{
  fTree.AddFocalHPhoton(copy, ekin  / GeV);
}
//--------------------------------------------------------------------
void
RootIO::AddFocalHHit(unsigned int copy,
		     double       energyLoss,
		     double       light,
		     int          nTrack,
		     int          pdg,
		     double       x,
		     double       y,
		     double       z,
		     double       t,
		     double       px,
		     double       py,
		     double       pz,
		     double       e)           
{
  fTree.AddFocalHHit(copy,        
		     energyLoss / GeV,  
		     light      / GeV,       
		     nTrack - 1,//Geant4 track numbers start at 1 !!!!      
		     pdg,         
		     x  / cm,           
		     y  / cm,           
		     z  / cm,           
		     t  / s,           
		     px / GeV,          
		     py / GeV,          
		     pz / GeV,          
		     e  / GeV);
}
//--------------------------------------------------------------------
void
RootIO::AddFocalEHit(unsigned int copy,
		     double       energyLoss,
		     int          nTrack,
		     int          pdg,
		     double       x,
		     double       y,
		     double       z,
		     double       t,
		     double       px,
		     double       py,
		     double       pz,
		     double       e)           
{
  fTree.AddFocalEHit(copy,        
		     energyLoss / GeV,  
		     nTrack - 1,//Geant4 track numbers start at 1 !!!!      
		     pdg,         
		     x  / cm,           
		     y  / cm,           
		     z  / cm,           
		     t  / s,           
		     px / GeV,          
		     py / GeV,          
		     pz / GeV,          
		     e  / GeV);
}
//--------------------------------------------------------------------
void
RootIO::AddFocalHFlux(int    nTrack, //Geant4 track numbers start at 1 !!!!
		      bool   front,
		      bool   back,
		      int    pdg,
		      double x,
		      double y,
		      double z,
		      double t,
		      double px,
		      double py,
		      double pz,
		      double e)           
{
  fTree.AddFocalHFlux(nTrack - 1,
		      front,
		      back,
		      pdg,         
		      x  / cm,           
		      y  / cm,           
		      z  / cm,           
		      t  / s,           
		      px / GeV,          
		      py / GeV,          
		      pz / GeV,          
		      e  / GeV);
}

//--------------------------------------------------------------------
void
RootIO::AddParticle(int    trackNo,//Geant4 track numbers start at 1 !!!!
		    int    pdg,
		    int    status,
		    int    parent, //Geant4 track numbers start at 1 !!!!
		    double x,
		    double y,
		    double z,
		    double t,
		    double px,
		    double py,
		    double pz,
		    double e)
{
  fTree.AddParticle(trackNo - 1,
		    pdg,
		    status,
		    parent - 1,
		    x  / cm,
		    y  / cm,
		    z  / cm,
		    t  / s,
		    px / GeV,
		    py / GeV,
		    pz / GeV,
		    e  / GeV);
}
  
//--------------------------------------------------------------------
void
RootIO::SetPrimary(int pdg,
		   double px,
		   double py,
		   double pz,
		   double e,
		   double vx,
		   double vy,
		   double vz,
		   double t)
{
  fTree.SetPrimary(pdg,
		   px / GeV,
		   py / GeV,
		   pz / GeV,
		   e  / GeV,
		   vx / cm, 
		   vy / cm, 
		   vz / cm, 
		   t  / s);
}
		

//--------------------------------------------------------------------
//
// EOF
//

  
