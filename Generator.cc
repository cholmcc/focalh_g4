//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Generator.cc
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Primary particle generator 
 */
#include "Generator.hh"
#include <G4ParticleTable.hh>
#include <G4ParticleGun.hh>
#include <Randomize.hh>
#include <iostream>

//--------------------------------------------------------------------
Generator::Generator(G4double       vtxz)
  : G4VUserPrimaryGeneratorAction(),
    fGun(new G4ParticleGun()),
    fVtxZ(vtxz)
{
  // These are set via command interface 
  // G4ParticleTable*      pt = G4ParticleTable::GetParticleTable();
  // G4ParticleDefinition* pd = pt->FindParticle(particle);
  // fGun->SetParticleDefinition(pd);
  // fGun->SetParticleMomentum(pz);
}

//--------------------------------------------------------------------
void
Generator::GeneratePrimaries(G4Event* event)
{
  G4RandGauss   g(*CLHEP::HepRandom::getTheEngine(),0,1*mm);
  G4double      vtxx = g.fire(); // 40 * (G4UniformRand()-.5) * mm;
  G4double      vtxy = g.fire(); // 40 * (G4UniformRand()-.5) * mm;
  G4ThreeVector pos(vtxx,vtxy,fVtxZ);
  G4ThreeVector md(0,0,1);
  fGun->SetParticleMomentumDirection(md);
  fGun->SetParticlePosition(pos);
  fGun->GeneratePrimaryVertex(event);

  G4cout << "Generated "
	 << fGun->GetParticleDefinition()->GetParticleName()
	 << " -> ("
	 << fGun->GetParticleMomentumDirection().x() << ","
	 << fGun->GetParticleMomentumDirection().y() << ","
	 << fGun->GetParticleMomentumDirection().z() << ") @ p="
	 << fGun->GetParticleMomentum() << " e=" 
	 << fGun->GetParticleEnergy() << G4endl;
    
}

//--------------------------------------------------------------------
//
// EOF
//


     
