//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    FocalEBuilder.hh
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Focal-E Geometry builder 
 */
#ifndef __FocalEBuilder__
#define __FocalEBuilder__
#include <G4SystemOfUnits.hh>
#include "FocalE.C"
#include <map>

class G4VPhysicalVolume;
class G4LogicalVolume;
class G4NistManager;
class G4Material;
class G4AssemblyVolume;
class G4Region;
class G4VSensitiveDetector;

//====================================================================
class FocalEBuilder
{
public:
  /** Constructor

      @param param Geometry paraemeters 
   */
  FocalEBuilder(const FocalE& param)
    : fParameters(param),
      fRegion(0)
  {}
  /** @{
      @name Interface methods */
  /** Create needed materials for the FocalH */
  void CreateMaterials(G4NistManager* manager);
  /** Build the actual volume */
  G4LogicalVolume* Construct(G4NistManager* nistm);
  /** Get mapping of senstive detectors to logical volumes */
  std::map<G4VSensitiveDetector*,G4LogicalVolume*> CreateSensitive();
  /** @} */
  /** @{
      @name Get information */
  /** Get length along Z */
  double   GetLength()   const { return fParameters.GetLength() * cm; }
  /** Get width of cobber tubes */
  double   GetWidth()    const { return fParameters.GetWidth() * cm; }
  /** Get height of cobber tubes */
  double   GetHeight()   const { return fParameters.GetHeight() * cm; }
  /** Get Pixel volume */
  G4LogicalVolume* GetPixelVolume() const { return fPxlVolume; }
  /** Get Pad volume */
  G4LogicalVolume* GetPadVolume() const { return fPadVolume; }
  /** Get the parameters */
  const FocalE& GetParameters() const { return fParameters; }
protected:
  G4LogicalVolume*  ConstructAbsorber(double         depth,
				      const char*    name,
				      G4NistManager* nistm);
  G4AssemblyVolume* ConstructPads(G4NistManager* nistm);
  G4AssemblyVolume* ConstructPixels(G4NistManager* nistm);

  /** Constant reference - cannot change */
  const FocalE& fParameters;
  /** Pad logical volume - stored to define sensitive detector */
  G4LogicalVolume*  fPadVolume;   
  /** Pixel logical volume - stored to define sensitive detector */
  G4LogicalVolume*  fPxlVolume;
  /** Focal-E region */
  G4Region*         fRegion;
};
#endif
//
// EOF
//

  
  
    
