//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    FrontBrack.cc
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Base classs of FrontBrack detectors
 */
#include "FrontBack.hh"
#include <G4Step.hh>
#include <G4Track.hh>
#include <G4SystemOfUnits.hh>
#include <G4ParticleTypes.hh>
#include <G4NavigationHistory.hh>

//____________________________________________________________________
std::pair<bool,bool>
FrontBack::IsEnterOrExit(G4Step* step) const
{
  G4StepPoint*      before    = step  ->GetPreStepPoint();
  G4StepPoint*      after     = step  ->GetPostStepPoint();
  G4bool            in1       = (before->GetTouchableHandle()
				 ->GetVolume()
				 ->GetLogicalVolume() == fVolume);
  G4bool            in2       = (after->GetTouchableHandle()
				 ->GetVolume()
				 ->GetLogicalVolume() == fVolume);
  G4bool            enter     = not in1 and in2;
  G4bool            exit      = in1 and not in2;
  return std::make_pair(enter,exit);
}

//____________________________________________________________________
std::pair<double,double>
FrontBack::LocalZ(G4Step* step, bool enter) const
{
  // Get Z coordiante of before and after step 
  G4StepPoint*            before    = step  ->GetPreStepPoint();
  G4StepPoint*            after     = step  ->GetPostStepPoint();
  G4ThreeVector           position1 = before->GetPosition();
  G4ThreeVector           position2 = after ->GetPosition();
  const G4TouchableHandle touch     = (enter ?
				       after->GetTouchableHandle() :
				       before->GetTouchableHandle());
  G4ThreeVector           local1    = (touch->GetHistory()
				       ->GetTopTransform()
				       .TransformPoint(position1));
  G4ThreeVector           local2    = (touch->GetHistory()
				       ->GetTopTransform()
				       .TransformPoint(position2));
  return std::make_pair(local1.z(),local2.z());
}
		  
//____________________________________________________________________
double
FrontBack::ToBack(G4Step* step) const
{
  auto enterExit  = IsEnterOrExit(step);
  bool enter      = enterExit.first;
  auto zs         = LocalZ(step, enter);
    
  G4double lbackZ = fLength / 2;

  return lbackZ - zs.first;
}

//____________________________________________________________________
std::pair<bool,bool>
FrontBack::IsFrontOrBack(G4Step* step) const
{
  auto enterExit = IsEnterOrExit(step);
  bool enter     = enterExit.first;
  bool exit      = enterExit.second;
  if (!enter and !exit) return std::make_pair(false,false);

  auto     zs        = LocalZ(step, enter);
  G4double z1        = zs.first;
  G4double z2        = zs.second;
  G4double lbackZ    = fLength / 2;
  G4double lfrontZ   = -fLength / 2;
  G4bool   lfront    = std::abs(z1 - lfrontZ) < .075 * mm;
  G4bool   lback     = std::abs(z2 - lbackZ)  < .075 * mm;

  return std::make_pair(lfront,lback);
}
//____________________________________________________________________
//
// EOF
//
