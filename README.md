# Geant 4 simulation of FoCAL-H testbeam 

This is a pure Geant 4 implementation of a simulation of the FoCAL-H
test-beam setup.   A similar simulation, based on VMC, can be found in
the  
[focalh_vmc](https://gitlab.com/cholmcc/focalh_vmc) project.

## Content 

- [`Geometry.hh`](Geometry.hh) and [`Geometry.cc`](Geometry.cc) -
  defines the class `Geometry` which defines the geometry of the
  setup.  It uses services from [`FocalH.C`](FocalH.C) to calculate
  positions and the like.  It also takes optical scintillation
  parameters from [`ScintOptical.C`](ScintOptical.C).
- [`OnRun.hh`](OnRun.hh) - defines the class `OnRun`.  In this setup,
  this does nothing.  Normally it would do stuff at start and end of
  run.
- [`OnEvent.hh`](OnEvent.hh) and [`OnEvent.cc`](OnEvent.cc) - defines
  the class `OnEvent`.  Objects of this class is responsible for
  clearing event storage before the start of an event, and flushing
  event data to disk at the end of the event.  This uses the singleton
  `RootIO`
  for this.  
- [`OnStep.hh`](OnStep.hh) and [`OnStep.cc`](OnStep.cc) - defines the
  class `OnStep`.  Objects of this class are invoked when ever the
  simulation deems that something happened.  This class will generate
  hits (objects of the class `Hit`, via the singleton `RootIO`) when
  ever we have a non-trivial hit in one of the scintillating fibres of
  FoCAL-H.
- [`Generator.hh`](Generator.hh) and [`Generator.cc`](Generator.cc) -
  defines the class `Generator`.  This is responsible for generating
  the primary particle of an event.  This uses a simple particle gun
  and starts at a vertex smeared in the transverse plane by a normal
  distribution.
- [`Initialisation.hh`](Initialisation.hh) - defines the class
  `Initialisation`.  An object of this class initialises objects of
  the classes `Generator`, `OnEvent`, `OnRun`, and `OnStep`.  Other
  than that, it plays no role.
- [`OpticalPhotonPhysics.hh`](OpticalPhotonPhysics.hh) - defines the
  class `OpticalPhotonPhysics` which loads a number of physics
  processes into the current physics list.
- [`RootIO.hh`](RootIO.hh) and [`RootIO.cc`](RootIO.cc) - a singleton
  wrapper around `EventTree` that allows global access to an
  `EventTree` object.  This is used in `OnEvent` and `OnStep`.  The
  main program sets up the singleton.
- [`EventTree.C`](EventTree.C) - defines the class `EventTree`.  This
  is a manager of the ROOT event tree and has methods for creating
  hits and the like.  The class can also be used to read back the
  generated data (see [`Read.C`](Read.C)).  
- [`Hit.C`](Hit.C) - defines the class `Hit`.  Objects of this class
  represents the information of a hit in a the FoCAL-H.  Objects of
  this class are stored in a ROOT `TTree` (branch `Hits`) via the
  `EventTree` class.  The information can be read back in ROOT (see
  [`Read.C`](Read.C)).
- [`FocalH.C`](FocalH.C) - defines the service class `FocalH`.  This
  can encode and decode column and row into and from copy numbers, as
  well as calculate the transverse local coordinates of a fibre.  This
  code is used throughout - and in the
  [focalh_vmc](https://gitlab.com/cholmcc/focalh_vmc) project - to
  ensure consistent numbering.
- [`LoadData.C`](Load.C) - a script to load data classes into ROOT. 
- [`Read.C`](Read.C) - an example of reading back data in ROOT.  To
  use this script, first run `Load.C` and then this - e.g., 
  
      root -l LoadData.C Read.C 

- [`main.cc`](main.cc) - the main program.  The program accepts a lot
  of options to configure the simulation.  Pass the option `--help` to
  see a summary of these options.
- [`macros`](macros) - this directory contains example macros.  The
  `main` program is set-up to read macros from this directory. 
- [`Makefile`](Makefile) - recipes to build the code. 
- [`README.md`](README.md) - this file 

We have also some code copied from the [VMC
project](https://gitlab.com/cholmcc/focalh_vmc) to analyse the result
of the simulation.   Yes, we can use the same code to analyse _both_
kinds of output. 

- [`Analyser.C`](Analyser.C) which defines the base class `Analyser`
  for defining analyses. 
- [`Analyse.C`](Analyse.C) which runs an analysis.  Use like 

	root -l Analyse.C 
	
- [`AnaConfig.C`](AnaConfig.C) that configures and analysis pass. 
- [`Profiler.C`](Profiler.C) which analyses the data for profiles of
  energy loss. 
  
When we have a digitisation analysis in the VMC project we can use it
here too. 

## Building 

Simply do 

	make 
	
To clean up, do 

	make clean 
	
This will remove all generated files, _except_ ROOT files.  To clean
these too, do 

	make realclean 

Note, Geant4 [has a
problem](https://bugzilla-geant4.kek.jp/show_bug.cgi?id=2242)  with
Wayland (the default GUI in most modern Linux distributions) and Qt.
Make sure you disable Qt when building Geant4. 

## Running 

_Do not sh*t were you eat_

The above is generally a good advice, also in the case of software.
What it means is that we do not put our messy stuff with the stuff we
want to work with.  That is, we want to keep our sources and the data
we generate with the sources separate from each other.  To that end,
this project provides the [`data`](data) sub-directory.   Change
directory to that sub-directory, and you should run the code from
there.

The [`data/Makefile`](data/Makefile) contains a number of targets to
help use the code.  Do

	make help 
	
to see more.

If you prefer, you can also run simulations _by-hand_.  For example 

	../main 
	
If you want to use a macro, e.g.,
[`macros/run.mac`](macros/run.mac) to set-up the job,
do

	../main -m run.mac 
	
(the sub-directory [`macros`](macros) is in the search path).

You can also pass options to control the simulation.  For example, 

	../main -n 100 -p 200 -t pi- 
	
to generate 100 events of $`\pi^-`$ with
$`p_z=200\,\mathrm{GeV}/\mathrm{c}`$.  Note, if you specify the number
of events with the option `-n` then _no_ macros are read.

You can also start an interactive session with 

	../main -i 
	
For more on the various options available, do 

	../main --help 
	
The default output file is `events.root`, which can be processed via
the class `EventTree` - see for example the ROOT script
[`Read.C`](Read.C).  Make sure you load the class definitions before
processing the data.  For example

	root Load.C Read.C 
	
(The [`data/.rootrc`](data/.rootrc) is set up so that ROOT will look
for scripts in the parent directory).

## Timing 

Running this example for 100 $`\pi^-`$ with
$`p_z=200\,\mathrm{GeV}/\mathrm{c}`$ gives 

    Command being timed: "../main -t pi- -p 200 -n 100"
    User time (seconds): 183.12
    System time (seconds): 0.31
    Percent of CPU this job got: 99%
    Elapsed (wall clock) time (h:mm:ss or m:ss): 3:03.62

or roughly 1.8 seconds per event. 

## Profiling 

To run the code with profiling, do for example 

	make 200_100_events.root PROFILE=1
	
and then investigate the profiling output with for example `hotspot`. 

## Other project 

Unlike the [focalh_vmc](https://gitlab.com/cholmcc/focalh_vmc)
project, simulations from this project does not include the full track
information from the event. 

  
