//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    FocalHHitter.cc
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Sensitive detector to make hits in scintillating fibres
 */
#include "FocalHHitter.hh"
#include <G4Step.hh>
#include <G4EmSaturation.hh>
#include <G4LossTableManager.hh>
#include <G4Track.hh>
#include <G4SystemOfUnits.hh>
#include <G4ParticleTypes.hh>
#include "RootIO.hh"

//____________________________________________________________________
FocalHHitter::FocalHHitter()
  : G4VSensitiveDetector("FocalHHitter"),
    fFirst(true)
{
}

//____________________________________________________________________
void
FocalHHitter::Initialize(G4HCofThisEvent *)
{
  if (!fFirst) return;

  fFirst = false;
  std::cout << "Initialize the FocalHHitter" << std::endl;
  RootIO::Instance()->MakeFocalHHits(isActive());
}

//____________________________________________________________________
G4bool
FocalHHitter::ProcessHits(G4Step* step, G4TouchableHistory*)
{
  // G4cout << "Hitter process step " << G4endl;
  G4double edep = step->GetTotalEnergyDeposit() / MeV;
  if (edep <= 0) return false;

  G4Track*          track    = step  ->GetTrack();
  if (track->GetDefinition() == G4OpticalPhoton::OpticalPhotonDefinition())
    return false;
  
  G4StepPoint*      before   = step  ->GetPreStepPoint();
  G4TouchableHandle touch    = before->GetTouchableHandle();
  G4int             copy     = touch ->GetCopyNumber();
  G4int             mcopy    = touch ->GetCopyNumber(2);
  //G4double          nedep    = step  ->GetNonIonizingEnergyDeposit() / MeV;
  //G4double          bare     = edep-nedep;
  G4EmSaturation*   sat      = G4LossTableManager::Instance()->EmSaturation();
  G4double          light    = sat->VisibleEnergyDepositionAtAStep(step) / MeV;
  G4ThreeVector     position = before->GetPosition();
  G4double          time     = before->GetGlobalTime();
  G4ThreeVector     momentum = before->GetMomentum();
  G4double          energy   = before->GetTotalEnergy();
  G4int             pdg      = track ->GetDynamicParticle()->GetPDGcode();
  G4int             trackNo  = track ->GetTrackID();
  RootIO::Instance()->AddFocalHHit(mcopy | copy,
				   edep,// bare?
				   light,
				   trackNo,
				   pdg,
				   position.x(),
				   position.y(),
				   position.z(),
				   time,
				   momentum.x(),
				   momentum.y(),
				   momentum.z(),
				   energy);
  return true;
}
//
// EOF
//

