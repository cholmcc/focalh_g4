//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Builder.cc
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Geometry builder 
 */
#include "Builder.hh"
#include "Fast.hh"
#include <G4AssemblyVolume.hh>
#include <G4Material.hh>
#include <G4NistManager.hh>
#include <G4Box.hh>
#include <G4Tubs.hh>
#include <G4LogicalVolume.hh>
#include <G4PVPlacement.hh>
#include <G4VPhysicalVolume.hh>
#include <G4ThreeVector.hh>
#include <G4RotationMatrix.hh>
#include <G4Transform3D.hh>
#include <G4NistManager.hh>
#include <G4VisAttributes.hh>
#include <G4Element.hh>
#include <G4Material.hh>
#include <G4SDManager.hh>
#include <G4VSensitiveDetector.hh>
#include <G4UImanager.hh>
#include "FocalHBuilder.hh"
#include "FocalEBuilder.hh"
#include "RootIO.hh"
#include "AirOptical.C"

// ===================================================================
double
Builder::GetCaveLength() const
{
  double maxZE = (fFocalE ? fFocalEPosition + fFocalE->GetLength() : 0);
  double maxZH = (fFocalH ? fFocalHPosition + fFocalH->GetLength() : 0);
  double ret   = 1.2 * 2 * std::max(maxZE, maxZH);
  std::cout << "Cave length\t" << maxZE << "\t" << maxZH << "\t-> "
	    << ret << std::endl;
  return ret;
}
// -------------------------------------------------------------------
double
Builder::GetCaveWidth() const
{
  double maxWE = fFocalE ? fFocalE->GetWidth() : 0;
  double maxWH = fFocalH ? fFocalH->GetWidth() : 0;
  double ret   = 1.2 * std::max(maxWE,maxWH);
  std::cout << "Cave width\t" << maxWE << "\t" << maxWH << "\t-> "
	    << ret << std::endl;
  return ret;
}
// -------------------------------------------------------------------
double
Builder::GetCaveHeight() const
{
  double maxHE = fFocalE ? fFocalE->GetHeight() : 0;
  double maxHH = fFocalH ? fFocalH->GetHeight() : 0;
  double ret   = 1.2 * std::max(maxHE,maxHH);
  std::cout << "Cave Height\t" << maxHE << "\t" << maxHH << "\t-> "
	    << ret << std::endl;
  return ret;
}

// ___________________________________________________________________
G4VPhysicalVolume*
Builder::Construct()
{
  double wrldW = GetCaveWidth()  / 2;
  double wrldH = GetCaveHeight() / 2;
  double wrldL = GetCaveLength() / 2;

  G4NistManager*   nistm   = G4NistManager::Instance();
  G4Box*           shpWrld = new G4Box("World",wrldW,wrldH,wrldL);
  G4Material*      air     = nistm->FindOrBuildMaterial("G4_AIR");
  G4LogicalVolume* volWrld = new G4LogicalVolume(shpWrld,air,"World");
  G4PVPlacement*   phsWrld = new G4PVPlacement(G4Transform3D(),"World",
					       volWrld, 0, false, 0);
  volWrld->SetVisAttributes(new G4VisAttributes(false,G4Color::Magenta()));


  CreateMaterials(nistm);

  if (fFocalH) {
    auto vol = fFocalH->Construct(nistm);
    auto mtx = new G4RotationMatrix();
    mtx->rotateX(0);//1.6*deg
    mtx->rotateY(0);
    mtx->rotateZ(0);
    G4ThreeVector     pos(0,0,fFocalHPosition+fFocalH->GetLength()/2);
    std::cout << "Placing Focal-H (" << fFocalH->GetLength()
	      << ") middle at " << pos << std::endl;
    new G4PVPlacement(mtx,
		      pos,
		      vol,
		      "FocalH",
		      volWrld,
		      false,
		      0);
  }
  if (fFocalE) {
    auto vol = fFocalE->Construct(nistm);
    auto mtx = new G4RotationMatrix();
    mtx->rotateX(0);//1.6*deg
    mtx->rotateY(0);
    mtx->rotateZ(0);
    G4ThreeVector     pos(0,0,fFocalEPosition+fFocalE->GetLength()/2);
    std::cout << "Placing Focal-E (" << fFocalE->GetLength()
	      << ") middle at " << pos << std::endl;
    new G4PVPlacement(mtx,
		      pos,
		      vol,
		      "FocalE",
		      volWrld,
		      false,
		      0);
  }
  RootIO::Instance()->AddParameter("FocalHPosition", fFocalHPosition);
  RootIO::Instance()->AddParameter("FocalEPosition", fFocalEPosition);
  auto ui = G4UImanager::GetUIpointer();
  ui->ApplyCommand("/persistency/gdml/write geometry.gdml");
  return phsWrld;
}

// ___________________________________________________________________
void
Builder::CreateMaterials(G4NistManager* nistm)
{

  G4Material*                air = nistm->FindOrBuildMaterial("G4_AIR");
  G4MaterialPropertiesTable* mt  = new G4MaterialPropertiesTable();
  mt->AddProperty("RINDEX",
		  AirOptical::kEnergies,
		  AirOptical::kRefractionIndex,
		  AirOptical::kNPoints);
  
  air->SetMaterialPropertiesTable(mt);

  // Create these here 
  new G4Element("Hydrogen","H", 1, 1.00794 * g/mole);
  new G4Element("Carbon","C",   6, 12.011 *  g/mole);
  
  if (fFocalH) fFocalH->CreateMaterials(nistm);
  if (fFocalE) fFocalE->CreateMaterials(nistm);

  // G4cout << *(G4Material::GetMaterialTable()) << G4endl;  
}  

// ___________________________________________________________________
void
Builder::ConstructSDandField()
{
  auto sm = G4SDManager::GetSDMpointer();

  if (fFocalH) {
    for (auto dv : fFocalH->CreateSensitive()) {
      sm->AddNewDetector(dv.first);
      SetSensitiveDetector(dv.second, dv.first);
    }
  }
  if (fFocalE) {
    for (auto dv : fFocalE->CreateSensitive()) {
      sm->AddNewDetector(dv.first);
      SetSensitiveDetector(dv.second, dv.first);
    }
  }
}


// ___________________________________________________________________
//
// EOF
//
 
