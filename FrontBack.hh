//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    FrontBrack.hh
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Base class for FrontBrack detectors
 */
#ifndef __FrontBack__
#define __FrontBack__
#include <G4SystemOfUnits.hh>
#include <map>
class G4LogicalVolume;
class G4Step;
class G4TouchableHistory;
class G4HCofThisEvent;

class FrontBack 
{
public:
  FrontBack(G4LogicalVolume* vol, double length)
    : fVolume(vol),
      fLength(length)
  {}
  /** Check if step is either at the front or back of volume

      @param step The step to check

      @return Pair of bools - first is front, second is back */
  std::pair<bool,bool> IsFrontOrBack(G4Step* step) const;
  /** Check if step corresponds to an entering or exiting step */
  std::pair<bool,bool> IsEnterOrExit(G4Step* step) const;
  /** Get local Z before and after step */
  std::pair<double,double> LocalZ(G4Step* step, bool enter) const;
  /** Get how far there is to the back from this step */
  double ToBack(G4Step* step) const;
protected:
  /** Our volume */
  G4LogicalVolume* fVolume;
  /** Length */
  double fLength;
};

#endif
//
// EOF
//
