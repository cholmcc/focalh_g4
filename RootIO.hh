//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    RootIO.hh
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Singleton wrapper around ROOT I/O
 */
#ifndef __RootIO__
#define __RootIO__
#include "EventTree.C"

/** Class to handle event I/O - wraps tree interface as a singleton,
    and translates Geant units (mm,ns,MeV) to ROOT units (cm,s,GeV) */
class RootIO
{
public:
  /** Get singleton */
  static RootIO* Instance();
  /** DTOR */
  ~RootIO()  {  Close();  }  
  /** Open this */
  void Open(const char* filename,
	    const char* treename,
	    const char* option="RECREATE")
  {
    fTree.Open(filename,treename,option);
  }
  /** Close file */  
  void Close() { fTree.Close(); }
  /** Set verbose */
  void SetVerbose(bool verbose=true)      { fTree.SetVerbose(verbose); }
  /** Set debug */
  void SetDebug(unsigned int debug=true)  { fTree.SetDebug(debug); }
  /** Set maxDepth */
  void SetKeepAll(unsigned int keepAll)   { fTree.SetKeepAll(keepAll); }  
  /** Set maxDepth */
  void SetMaxDepth(unsigned int maxDepth) { fTree.SetMaxDepth(maxDepth); }
  /** Set wehter we make hits */
  void MakeFocalHHits(bool enable) { fTree.SetupFocalHHitsBranch(enable); }
  /** Set wehter we make hits */
  void MakeFocalEHits(bool enable) { fTree.SetupFocalEHitsBranch(enable); }
  /** Set wehter we make sums */
  void MakeFocalHSums(bool enable, const FocalH* param) {
    fTree.SetupFocalHSumsBranch(enable, param); }
  /** Set wehter we make flux */
  void MakeFocalHFlux(bool enable) { fTree.SetupFocalHFluxBranch(enable); }
  /** Set wehter we make particle */
  void MakeParticles(bool enable=true) { fTree.SetupParticleBranch(enable); }
  /** Set wehter we make particle */
  void MakePrimary(bool enable=true) { fTree.SetupPrimaryBranch(enable); }
  /** Fill into tree */
  void   Fill() { fTree.Fill(); }
  /** Clear event structures */
  void   Clear() { fTree.Clear(); }
  /** Addd a hit

      @param copy        Straw copy number
      @param energyLoss  Energy deposition in MeV
      @param light       Photon yield energy in MeV
      @param nTrack      Track number, starting from 1
      @param pdg         Particle type 
      @param x           X-coordinate of hit in mm
      @param y           Y-coordinate of hit in mm
      @param z           Z-coordinate of hit in mm
      @param t           Time of hit in ns
      @param px          X-Momentum in MeV
      @param py          Y-Momentum in MeV
      @param pz          Z-Momentum in MeV
      @param e           Total energy in MeV
   */
  void AddFocalHHit(unsigned int copy,
		    double       energyLoss,
		    double       light,
		    int          nTrack, //Geant4 track numbers start at 1 !!!!
		    int          pdg,
		    double       x,
		    double       y,
		    double       z,
		    double       t,
		    double       px,
		    double       py,
		    double       pz,
		    double       e);
  /** Addd a hit

      @param copy        Straw copy number
      @param energyLoss  Energy deposition in MeV
      @param light       Photon yield energy in MeV
      @param nTrack      Track number, starting from 1
      @param pdg         Particle type 
      @param x           X-coordinate of hit in mm
      @param y           Y-coordinate of hit in mm
      @param z           Z-coordinate of hit in mm
      @param t           Time of hit in ns
      @param px          X-Momentum in MeV
      @param py          Y-Momentum in MeV
      @param pz          Z-Momentum in MeV
      @param e           Total energy in MeV
   */
  void AddFocalEHit(unsigned int copy,
		    double       energyLoss,
		    int          nTrack, //Geant4 track numbers start at 1 !!!!
		    int          pdg,
		    double       x,
		    double       y,
		    double       z,
		    double       t,
		    double       px,
		    double       py,
		    double       pz,
		    double       e);
  /** Addd a flux hit

      @param nTrack      Track number, starting from 1
      @param front       Enter 
      @param back        Exit
      @param pdg         Particle type 
      @param x           X-coordinate of hit in mm
      @param y           Y-coordinate of hit in mm
      @param z           Z-coordinate of hit in mm
      @param t           Time of hit in ns
      @param px          X-Momentum in MeV
      @param py          Y-Momentum in MeV
      @param pz          Z-Momentum in MeV
      @param e           Total energy in MeV
   */
  void AddFocalHFlux(int    nTrack, //Geant4 track numbers start at 1 !!!!
		     bool   front,
		     bool   back,
		     int    pdg,
		     double x,
		     double y,
		     double z,
		     double t,
		     double px,
		     double py,
		     double pz,
		     double e);
  //------------------------------------------------------------------
  /** Add summed signal
      @param copy  Copy number
      @param edep  Energy loss in GeV
      @param light Light yield in GeV
  */
  void AddFocalHSum(unsigned int copy,
		    double       edep,
		    double       light);
  //------------------------------------------------------------------
  /** Add photon signal
      @param copy  Copy number
      @param ekin  Kinetic energy in GeV
  */
  void AddFocalHPhoton(unsigned int copy,
		       double       ekin);
  //------------------------------------------------------------------
  /** Add a particle
      
      @param trackNo     Track number - starts at 1
      @param status      Status code
      @param parent      Parent track - starts at 1
      @param pdg         Particle type
      @param x           X-coordinate of hit in mm
      @param y           Y-coordinate of hit in mm
      @param z           Z-coordinate of hit in mm
      @param t           Time of hit in ns
      @param px          X-Momentum in MeV
      @param py          Y-Momentum in MeV
      @param pz          Z-Momentum in MeV
      @param e           Total energy in MeV
   */
  void AddParticle(int    trackNo,//Geant4 track numbers start at 1 !!!!
		   int    pdg,
		   int    status,
		   int    parent, //Geant4 track numbers start at 1 !!!!
		   double x,
		   double y,
		   double z,
		   double t,
		   double px,
		   double py,
		   double pz,
		   double e);
  /** Set primary particle

      @param pdg         Particle type
      @param x           X-coordinate of hit in mm
      @param y           Y-coordinate of hit in mm
      @param z           Z-coordinate of hit in mm
      @param t           Time of hit in ns
      @param px          X-Momentum in MeV
      @param py          Y-Momentum in MeV
      @param pz          Z-Momentum in MeV
      @param e           Total energy in MeV
   */
  void SetPrimary(int pdg,
		  double px,
		  double py,
		  double pz,
		  double e,
		  double vx,
		  double vy,
		  double vz,
		  double t);
  //------------------------------------------------------------------
  void AddParameter(const char* name, int value)
  {
    fTree.AddParameter(name,value);
  }
  //------------------------------------------------------------------
  void AddParameter(const char* name, double value)
  {
    fTree.AddParameter(name,value);
  }
  //------------------------------------------------------------------
  void Write(const TObject* obj, const char* name)
  {
    fTree.Write(obj,name);
  }
  //------------------------------------------------------------------
  TObject* Read(const char* name)
  {
    return fTree.Read(name);
  }
protected:
  /** CTOR */
  RootIO();
  /*** single ton */
  static RootIO* fgInstance;
  /** Real tree */
  EventTree fTree;
};

#endif
//
// EOF
//
