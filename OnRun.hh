//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    OnRun.cc
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Handle a new run
 */
#ifndef __OnRun__
#define __OnRun__
#include <G4UserRunAction.hh>
#include <G4AnalysisManager.hh>
#include <G4Run.hh>
// class G4Run;

/** Start and end of runs - could initialize and close ROOT output */
class OnRun : public G4UserRunAction
{
public:
  OnRun() : G4UserRunAction() {}
  void BeginOfRunAction(const G4Run* run)
  {
    G4cout << "Run # " << run->GetRunID() << G4endl;
  }
  void EndOfRunAction(const G4Run* run)
  {
    G4cout << "End of run # " << run->GetRunID() << G4endl;
  }
};

#endif
//
// EOF
//

    
