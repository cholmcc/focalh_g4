//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Builder.hh
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Geometry builder 
 */
#ifndef __Builder__
#define __Builder__
#include <G4VUserDetectorConstruction.hh>
#include <G4SystemOfUnits.hh>

class G4VPhysicalVolume;
class G4LogicalVolume;
class G4NistManager;
class G4Material;
class G4AssemblyVolume;
class G4Region;

class FocalHBuilder;
class FocalEBuilder;

//====================================================================
class Builder : public G4VUserDetectorConstruction
{
public:
  /** CTOR */
  Builder(FocalHBuilder* focalH=0, double focalHpos=0,
	  FocalEBuilder* focalE=0, double focalEpos=0)
    : fFocalH(focalH),
      fFocalE(focalE),
      fFocalHPosition(focalHpos),
      fFocalEPosition(focalEpos)
  {}
  /** DTOR */
  ~Builder() {}
  /** Construct geometry and return top physical volume */
  G4VPhysicalVolume* Construct();
  /** Construct senstive detectors and magnetic field */
  void               ConstructSDandField();
  
  double   GetCaveLength() const; 
  double   GetCaveWidth() const; 
  double   GetCaveHeight() const; 
protected:
  FocalHBuilder*    fFocalH;
  FocalEBuilder*    fFocalE;
  double            fFocalHPosition;
  double            fFocalEPosition;

  /** Delegate from construct */
  void CreateMaterials(G4NistManager* manager);
};

#endif
//
// EOF
//

  
  
    
