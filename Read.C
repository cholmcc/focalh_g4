//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Read.C
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Example of reading from data tree
 */
#include "DrawXYProjections.C"
#include <TPDGCode.h>

void Read(const char* filename="events.root",Int_t nev=-1,
	  const char* treename="T")
{
  Double_t emax = .04;
  Double_t sumf = 15;
  TH2* lightVsEdep = new TH2D("lightEdep","Light vs Energy loss response",
		     400, 0, emax, 400, 0, emax);
  lightVsEdep->SetXTitle("#Delta E (GeV)");
  lightVsEdep->SetYTitle("#gamma (GeV)");
  TH1* diffLightEdep = new TH1D("diff","Difference", 100,-emax/2,emax/2);
  diffLightEdep->SetXTitle("#Delta E - #gamma (GeV)");

  TH2* sumLightVsEdep = new TH2D("sumLightEdep",
				 "Summed Light vs Energy loss response",
				 400, 0, sumf*emax, 400, 0, sumf*emax);
  sumLightVsEdep->SetXTitle("#Delta E (GeV)");
  sumLightVsEdep->SetYTitle("#gamma (GeV)");
  TH1* sumDiffLightEdep = new TH1D("sumDiff","Difference",
				   100,
				   -emax*sumf/2,
				   +emax*sumf/2);
  sumDiffLightEdep->SetXTitle("#Delta E - #gamma (GeV)");

  std::map<std::string,Int_t> names = { {"#gamma",     kGamma},
					{"e^{#pm}",    kElectron},
					{"#mu^{#pm}",  kMuonMinus},
					{"#pi^{#pm}",  kPiPlus},
					{"#pi^{0}",    kPi0},
					{"n",          kNeutron},
					{"p/#bar{p}",  kProton},
					{"Other",      0}  };
  std::map<Int_t,Int_t> binMap;
  TH1* pdg = new TH1D("pdgOut","Particles leaving FoCAL-H",
		      names.size(), 0, names.size());
  pdg->SetFillColor(kRed+1);
  pdg->SetFillStyle(3001);
  pdg->SetYTitle("#LTN#GT");
  Int_t bin = 1;
  for (auto nc : names) {
    pdg->GetXaxis()->SetBinLabel(bin,nc.first.c_str());
    binMap[ nc.second] = bin;
    binMap[-nc.second] = bin;
    bin++;
  }
  
  EventTree t;
  t.Open(filename,treename,"READ");
  t.SetupHitsBranch(true);
  t.SetupFocalHSumsBranch(true);
  t.SetupFocalHFluxBranch(true);
  t.SetupParticleBranch(true);
  // t.SetupPrimaryBranch(true);
  

  int ev = 0;
  while (t.GetEntry(ev++) > 0) {
    if (nev > 0 and ev > nev) break;
    // t.GetPrimary()->Print();

    TClonesArray* hits   = t.GetHits();
    TClonesArray* sums   = t.GetFocalHSums();
    TClonesArray* fluxes = t.GetFocalHFluxes();
    Printf("Event %6d,  %6d hits",ev,hits->GetEntries());

    for (auto o : *hits) {
      Hit* hit = static_cast<Hit*>(o);
      if (hit->fEnergyLoss <= 0 and hit->fLight <= 0) continue;
      lightVsEdep  ->Fill(hit->fEnergyLoss,  hit->fLight);
      diffLightEdep->Fill(hit->fEnergyLoss - hit->fLight);

      // To get particle corresponding to the hit
      // TParticle* p = t.GetParticle(hit->NTrack());
    }

    for (auto o : *sums) {
      FocalHSum* sum = static_cast<FocalHSum*>(o);
      if (sum->fEnergyLoss <= 0 and sum->fLight <= 0) continue;
      sumLightVsEdep  ->Fill(sum->fEnergyLoss,  sum->fLight);
      sumDiffLightEdep->Fill(sum->fEnergyLoss - sum->fLight);
    }
    for (auto o : *fluxes) {
      FocalHFlux* flux = static_cast<FocalHFlux*>(o);
      if (!flux->IsExit()) continue;
      
      auto it = binMap.find(flux->Pdg());
      if (it == binMap.end()) it = binMap.find(0);

      pdg->Fill(it->second);
    }
    
    // To loop over particles
    // for (auto o : *t.GetParticles()) {
    //   TParticle* p = static_cast<TParticle*>(o);
    // }
  }

  t.Close();

  gStyle->SetOptStat(0);

  // A little bit of fancy drawing :-)
  TCanvas* cHits = new TCanvas("cHits","CHits",900,800);
  cHits->SetTopMargin  (0.08);
  cHits->SetRightMargin(0.12);
  cHits->SetLeftMargin (0.14);
  cHits->SetGridx();
  cHits->SetGridy();
  cHits->SetLogz();
  
  TVirtualPad* ph = DrawXYProjections(cHits, lightVsEdep, "COLZ",
				      .7, .7, 0.01,
				      "",
				      "",
				      true);

  diffLightEdep->Draw();
  AdjustAxis(diffLightEdep->GetXaxis(), .7);
  AdjustAxis(diffLightEdep->GetYaxis(), .7);

  // A little bit of fancy drawing :-)
  TCanvas* cSums = new TCanvas("sums","Sums",900,800);
  cSums->SetTopMargin  (0.08);
  cSums->SetRightMargin(0.12);
  cSums->SetLeftMargin (0.14);
  cSums->SetGridx();
  cSums->SetGridy();
  cSums->SetLogz();
  
  TVirtualPad* ps = DrawXYProjections(cSums, sumLightVsEdep, "COLZ",
				     .7, .7, 0.01,
				     "",
				     "",
				     true);

  sumDiffLightEdep->Draw();
  AdjustAxis(sumDiffLightEdep->GetXaxis(), .7);
  AdjustAxis(sumDiffLightEdep->GetYaxis(), .7);

  TCanvas* cPdg = new TCanvas("cPdg","PDG codes of particles leaving",
			      900,800);
  cPdg->SetRightMargin(0.01);
  cPdg->cd();
  pdg->Scale(1./ev);
  pdg->Draw("E");
  pdg->Draw("hist same");

  TString outN = TString(filename).ReplaceAll("events","light_edep");
  TFile* file = TFile::Open(outN,"RECREATE");
  cHits->Write();
  cSums->Write();
  lightVsEdep->Write();
  diffLightEdep->Write();
  sumLightVsEdep->Write();
  sumDiffLightEdep->Write();
  pdg->Write();
  lightVsEdep->SetDirectory(0);
  diffLightEdep->SetDirectory(0);
  sumLightVsEdep->SetDirectory(0);
  sumDiffLightEdep->SetDirectory(0);
  pdg->SetDirectory(0);
  file->Write();
  file->Close(); 
    
}
//
// EOF
//
