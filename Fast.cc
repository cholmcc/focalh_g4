//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Fast.cc
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Fast optical photon simulation
 */
#include "Fast.hh"
#include <G4ParticleDefinition.hh>
#include <G4ParticleTypes.hh>
#include <G4GeometryTolerance.hh>
#include <G4ProcessManager.hh>
#include "G4OpProcessSubType.hh"
#include <G4OpBoundaryProcess.hh>
#include <G4OpAbsorption.hh>
#include <G4OpWLS.hh>
#include <G4Tubs.hh>
#include <iomanip>

//____________________________________________________________________
G4bool
Fast::Data::IsRepetitive(const Data& other, bool alsoStep) const
{
  if (fTrackNo  != other.fTrackNo) return false;
  if (fBoundary != other.fBoundary) return false;
  if (alsoStep &&
      std::abs(fStepLength-other.fStepLength) >
      G4GeometryTolerance::GetInstance()->GetSurfaceTolerance())
    return false;
  return true;
}
//____________________________________________________________________
std::ostream& operator<<(std::ostream& o, const Fast::Data& d)
{
  return o << std::setw(6) << d.fTrackNo       << ": "
	   << std::setw(6) << d.fKineticEnergy << " MeV "
	   << std::setw(6) << d.fGlobalTime    << " ns "
	   << std::setw(6) << d.fPathLength    << " mm "
	   << std::setw(6) << d.fStepLength    << " mm "
	   << std::setw(3) << d.fBoundary      << " "
	   << std::setw(6) << d.fAbsorption    << " "
	   << std::setw(6) << d.fWLS           << " "
	   << std::setw(6) << d.fPosition      << "\t"
	   << std::setw(6) << d.fMomentum;
}


//____________________________________________________________________
G4bool
Fast::IsApplicable(const  G4ParticleDefinition& type)
{
  return &type == G4OpticalPhoton::OpticalPhotonDefinition();
}
//____________________________________________________________________
G4bool
Fast::ModelTrigger(const G4FastTrack& fastTrack)
{
  // Get the track 
  const G4Track* track = fastTrack.GetPrimaryTrack();
  //G4int          pdg   = track ->GetDynamicParticle()->GetPDGcode();

  // reset when moving to the next track
  if (fCurrent.fTrackNo != track->GetTrackID()) {
    // std::cout << "Track " << track->GetTrackID()
    // 	      << " " << pdg 
    // 	      << " not same as current "
    // 	      << fCurrent.fTrackNo
    // 	      << std::endl;
    Reset();
  }

  // Make sure that the track does not get absorbed after
  // transportation, as number of interaction length left is reset
  // when doing transportation
  if (!Check()) {
    // track is already transported but did not pass NILL check,
    // attempt to reset NILL
    return true;
  }

  // Track is already transported and did pass NILL check, nothing to do
  if (fTransported) {
    // std::cout << "Track already transported" << std::endl;
    if (fAxis.dot(track->GetMomentumDirection()) *
	fAxis.dot(fCurrent.fMomentum) < 0)
      // different propagation direction (e.g. mirror)
      Reset();

    return false;
  }

  if (!CheckTotalInternalReflection(track)) {
    // nothing to do if the track has no repetitive total internal
    // reflection
    return false;
  }

  auto touchable = track->GetTouchableHandle();
  auto solid     = touchable->GetSolid();

  if (solid->GetEntityType() != "G4Tubs" ) {
    G4cout << "Solid is not a tube but a " << solid->GetEntityType()
	   << std::endl;
    return false; // only works for G4Tubs at the moment
  }
  
  G4Tubs*  tubs   = static_cast<G4Tubs*>(solid);
  G4double length = 2.*tubs->GetZHalfLength();

  // Get some geometry information - point in Z
  G4ThreeVector position = (touchable->GetHistory()
			    ->GetTopTransform()
			    .Inverse()
			    .TransformPoint(G4ThreeVector(0.,0.,0.)));
  fAxis                  = (touchable->GetHistory()
			    ->GetTopTransform()
			    .Inverse()
			    .TransformAxis(G4ThreeVector(0.,0.,1.)));

  // The vector it is traveling along, and projection onto the axis of
  // the tube.
  auto delta  = fCurrent.fPosition - fPrevious.fPosition;
  fTransportZ = delta.dot(fAxis);

  // Estimate the number of expected total internal reflections before
  // reaching fiber end
  auto back             = (position+(fTransportZ > 0 ? 1 : -1)*fAxis*length/2);
  auto remain           = back - track->GetPosition();
  G4double remainZ      = remain.dot(fAxis);
  G4double maxTransport = std::floor(remainZ/fTransportZ);
  fNTransport           = maxTransport - fSafety;
  // std::cout << "current=" << position
  // 	    << " end=" << back
  // 	    << " remain=" << remainZ
  // 	    << " max=" << maxTransport
  // 	    << " steps=" << fNTransport
  // 	    << std::endl;

  if (fNTransport < 1)
    // Require at least n = fSafety of total internal reflections at the end
    return false; 

  if (CheckAbsorption(fPrevious.fWLS, fCurrent.fWLS)){ 
    // Do nothing if WLS happens before reaching fiber end
    // G4cout << "Wave-length shift before end" << std::endl;
    return false;
  }
  if (CheckAbsorption(fPrevious.fAbsorption, fCurrent.fAbsorption)) {
    // Absorbed before reaching fiber end
    // G4cout << "Absorbtion before end" << std::endl;
    fKill = true;
  }

  return true;
}

//____________________________________________________________________
void
Fast::DoIt(const G4FastTrack& fastTrack, G4FastStep& fastStep) {
  auto track = fastTrack.GetPrimaryTrack();

  if (fKill) {
    // G4cout << "Kill track" << std::endl;
    // Track was absorbed, kill it 
    fastStep.ProposeTotalEnergyDeposited(track->GetKineticEnergy());
    fastStep.KillPrimaryTrack();

    return;
  }

  if (fTransported)
    // Reset NILL if the track did not meet NILL check
    return; 

  // Get current time
  auto   pos       = track->GetPosition();
  auto   time      = track->GetGlobalTime();
  double timeZ     = fCurrent.fGlobalTime - fPrevious.fGlobalTime;
  auto   posShift  = fTransportZ * fNTransport * fAxis;
  double timeShift = timeZ * fNTransport;
  if (fNTransport > 5)
    G4cout << "Transport track " << track->GetTrackID()
	   << " from=" << pos << "," << time 
	   << " to=" << pos+posShift << "," << time+timeShift
	   << " " << fTransportZ << " " << fNTransport 
	   << std::endl;

  fastStep.ProposePrimaryTrackFinalPosition(pos  + posShift, false );
  fastStep.ProposePrimaryTrackFinalTime    (time + timeShift);
  fastStep.ProposePrimaryTrackFinalKineticEnergy(track->GetKineticEnergy());
  fastStep.ProposePrimaryTrackFinalMomentumDirection(track
						     ->GetMomentumDirection(),
						     false);
  fastStep.ProposePrimaryTrackFinalPolarization(track->GetPolarization(),
						false);
  fTransported = true;

  return;
}

//____________________________________________________________________
G4bool
Fast::CheckTotalInternalReflection(const G4Track* track)
{
  SetPostStepProcesses(track);

  // If track is stopped somehow 
  if (track->GetTrackStatus()==fStopButAlive ||
      track->GetTrackStatus()==fStopAndKill) {
    G4cout << "Track " << track->GetTrackID()
	   << " is stopped "
	   << (track->GetTrackStatus()==fStopButAlive ? "but alive" : "")
	   << (track->GetTrackStatus()==fStopAndKill  ? "and dead" : "")
	   << std::endl;
    return false;
  }

  // accumulate step length
  fCurrent += track->GetStepLength();

  G4int status = fBoundary->GetStatus();
  // G4cout << status << " ["
  // 	 << G4OpBoundaryProcessStatus::FresnelReflection << ","
  // 	 << G4OpBoundaryProcessStatus::TotalInternalReflection << "," 
  // 	 << G4OpBoundaryProcessStatus::StepTooSmall << "]"
  // 	 << std::endl;
  // skip exceptional iteration with FresnelReflection
  if (status == G4OpBoundaryProcessStatus::FresnelReflection) {
    //G4cout << " " << track->GetTrackID() << " Fresnel reflection" << std::endl;
    fCurrent.fBoundary = status;
  }
  
  // some cases have a status StepTooSmall when the reflection happens
  // between the boundary of cladding & outer volume
  // (outside->cladding) since the outer volume is not a G4Region
  if (status == G4OpBoundaryProcessStatus::TotalInternalReflection ||
      status == G4OpBoundaryProcessStatus::StepTooSmall) {
    // G4cout << " Total reflection or too small step" << std::endl;
    if (status == G4OpBoundaryProcessStatus::TotalInternalReflection) {
      // skip StepTooSmall if the track already has TotalInternalReflection
      // G4cout << "  Total reflection" << std::endl;
      if (fCurrent.fBoundary ==
	  G4OpBoundaryProcessStatus::TotalInternalReflection) {
	//G4cout << "  Total reflection at boundary on current" << std::endl;
        return false;
      }
      if (fPrevious.fBoundary ==
	  G4OpBoundaryProcessStatus::TotalInternalReflection) {
	//G4cout << "  Total reflection at boundary on previous" << std::endl;	
        return false;
      }
    }

    Data candidate;
    candidate = *track;
    candidate.fBoundary = status;
    if (fAbsorption)
      candidate.fAbsorption = fAbsorption->GetNumberOfInteractionLengthLeft();
    if (fWLS)
      candidate.fWLS        = fWLS->GetNumberOfInteractionLengthLeft();

    G4bool repetitive = false;
    if (candidate.IsRepetitive(fCurrent,false) &&
	fCurrent.IsRepetitive(fPrevious))
      repetitive = true;

    // if (repetitive)
    //   G4cout << candidate << std::endl;
    
    fPrevious = fCurrent;
    fCurrent  = candidate;

    return repetitive;
  }

  return false;
}

//____________________________________________________________________
G4bool
Fast::CheckAbsorption(G4double prev, G4double current) const
{
  if (prev < 0 || current < 0)
    // The number of interaction length left has to be reset
    return false; 
  if (prev == current)
    // no absorption
    return false; 
  if (prev    == std::numeric_limits<double>::max() ||
      current == std::numeric_limits<double>::max())
    // NILL is re-initialized
    return false; 

  G4double delta = prev - current;

  if (current - delta * (fNTransport + fSafety) < 0)
    // absorbed before reaching fiber end
    return true; 

  return false;
}

//____________________________________________________________________
// If this returns false, then reset 
G4bool
Fast::Check() const
{
  if (fTransported)
    return true; // do nothing if the track is not already transported

  G4double absorption = std::numeric_limits<double>::max();
  G4double wls        = std::numeric_limits<double>::max();

  // Check if we have absorption
  if (fWLS) {
    if (fPrevious.fWLS  == std::numeric_limits<double>::max() ||
	fCurrent .fWLS  == std::numeric_limits<double>::max())
      return true; // Reinitialize 
    wls = fWLS->GetNumberOfInteractionLengthLeft();
  }
	
  if (fAbsorption) {
    if (fPrevious.fAbsorption == std::numeric_limits<double>::max() ||
	fCurrent .fAbsorption  == std::numeric_limits<double>::max())
      return true; // NILL is re-initialized
    absorption = fAbsorption->GetNumberOfInteractionLengthLeft();
  }

  if (wls < 0 || absorption < 0)
    return true; // let GEANT4 to reset them

  G4double deltaWLS        = fPrevious.fWLS        - fCurrent.fWLS;
  G4double deltaAbsorption = fPrevious.fAbsorption - fCurrent.fAbsorption;
  G4double finalWLS        = wls        - deltaWLS        * fSafety;
  G4double finalAbsorption = absorption - deltaAbsorption * fSafety;

  // prevent double counting of the probability of getting absorbed
  // (which already estimated before transportation) reset NILL again
  if (finalWLS < 0 || finalAbsorption < 0)
    return false;

  return true;
}

//____________________________________________________________________
void
Fast::SetPostStepProcesses(const G4Track* track)
{
  if (fGotProcess) return;
  
  G4ProcessManager* pm = track->GetDefinition()->GetProcessManager();
  auto& processes = *pm->GetPostStepProcessVector();

  for (size_t i = 0; i < processes.size(); i++) {
    auto& process = processes[i];
    auto  type    = process->GetProcessType();
    if (type != fOptical) continue;
    
    auto sub = process->GetProcessSubType();
    if      (sub == G4OpProcessSubType::fOpBoundary) {
      G4cout << "Got boundary process" << std::endl;
      fBoundary = static_cast<G4OpBoundaryProcess*>(process);
    }
    else if (sub == G4OpProcessSubType::fOpAbsorption) {
      G4cout << "Got absorbtion process" << std::endl;
      fAbsorption = static_cast<G4OpAbsorption*>(process);
    }
    else if (sub == G4OpProcessSubType::fOpWLS)  {
      G4cout << "Got wave-length shift process" << std::endl;
      fWLS = static_cast<G4OpWLS*>(process);
    }
  }
  fGotProcess = true;
  
  return;
}

//____________________________________________________________________
void
Fast::Reset()
{
  fNTransport  = 0;
  fTransportZ  = 0;
  fAxis        = G4ThreeVector(0); // Axis we're transporting along 
  fKill        = false; // Whether track should be killed 
  fTransported = false; // Whether track was fast-moved 
  fCurrent .Reset();
  fPrevious.Reset();
}
//
// EOF
//
