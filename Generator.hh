//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Generator.hh
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Primary particle generator 
 */
#ifndef __Generator__
#define __Generator__
#include <G4VUserPrimaryGeneratorAction.hh>
#include <G4SystemOfUnits.hh>
#include <G4String.hh>
class G4Event;
class G4ParticleGun;

class Generator : public G4VUserPrimaryGeneratorAction
{
public:
  Generator(G4double vtxZ=0);
  
  void GeneratePrimaries(G4Event* event);

protected:
  G4ParticleGun* fGun;
  G4double       fVtxZ;
};

#endif
//
// EOF
//
