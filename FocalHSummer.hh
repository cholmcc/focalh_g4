//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    FocalHSummer.hh
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   FrontBrack detector to make hits in scintillating fibres
 */
#ifndef __FocalHSummer__
#define __FocalHSummer__
#include <G4VSensitiveDetector.hh>
#include <G4SystemOfUnits.hh>
#include "FrontBack.hh"
#include "FocalH.C"

class FocalHSummer : public G4VSensitiveDetector, public FrontBack
{
public:
  /** Constructor */
  FocalHSummer(G4LogicalVolume* vol, G4double length, const FocalH& param)
    : G4VSensitiveDetector("FocalHSummer"),
      FrontBack(vol, length),
      fParam(param),
      fMaxTime(param.fMaxTime * s),
      fFirst(true)
  {}
  /** Initialize on each event - does nothing(?) */
  void Initialize(G4HCofThisEvent*);
  /** Process a step */
  G4bool ProcessHits(G4Step* step, G4TouchableHistory* notUsed);
protected:
  G4bool ProcessOptical(G4Step* step);
  const FocalH&  fParam;
  G4double       fMaxTime;
  G4bool         fFirst;
};

#endif
//
// EOF
//

