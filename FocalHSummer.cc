//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    FocalHSummer.cc
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Sensitive detector to make hits in scintillating fibres
 */
#include "FocalHSummer.hh"
#include <G4Step.hh>
#include <G4EmSaturation.hh>
#include <G4LossTableManager.hh>
#include <G4Track.hh>
#include <G4PhysicalConstants.hh>
#include <G4ParticleTypes.hh>
#include "RootIO.hh"

//____________________________________________________________________
void
FocalHSummer::Initialize(G4HCofThisEvent *)
{
  if (!fFirst) return;

  fFirst = false;
  std::cout << "Initialize the FocalHSummer" << std::endl;
  RootIO::Instance()->MakeFocalHSums(isActive(),&fParam);
}


//____________________________________________________________________
G4bool
FocalHSummer::ProcessHits(G4Step* step, G4TouchableHistory*)
{
  G4Track*          track    = step  ->GetTrack();
  if (track->GetDefinition() == G4OpticalPhoton::OpticalPhotonDefinition()) 
    return ProcessOptical(step);  

  // G4cout << "Summer process step " << G4endl;
  G4double edep = step->GetTotalEnergyDeposit() / MeV;
  if (edep <= 0) return false;

  G4StepPoint*      before   = step  ->GetPreStepPoint();
  G4TouchableHandle touch    = before->GetTouchableHandle();
  G4int             copy     = touch ->GetCopyNumber();
  G4int             mcopy    = touch ->GetCopyNumber(2);
  //G4double          nedep    = step  ->GetNonIonizingEnergyDeposit() / MeV;
  //G4double          bare     = edep-nedep;
  G4EmSaturation*   sat      = G4LossTableManager::Instance()->EmSaturation();
  G4double          light    = sat->VisibleEnergyDepositionAtAStep(step) / MeV;
  // This should be done in the local coordinate system
  G4double          time     = before->GetGlobalTime();
  G4double          zrem     = ToBack(step);
  G4double          dt       = zrem / c_light;
  if ((time + dt) > fMaxTime) {
    // G4cout << "Time " << time/s << " + " << zrem/cm << "/c = "
    //        << time/s << " + " << zrem/cm/(c_light/cm*s) << " = "
    //        << (time+dt)/s << " > " << fMaxTime / s << " energy loss "
    //        << edep << " and light " << light << " not added"
    //        << G4endl;
    return false;
  }
  
  RootIO::Instance()->AddFocalHSum(mcopy | copy,
				   edep,// bare?
				   light);

  return true;
}

//____________________________________________________________________
G4bool
FocalHSummer::ProcessOptical(G4Step* step)
{
  // This should be done in the local coordinate system 
  auto frontBack = IsFrontOrBack(step);
  bool back      = frontBack.second;
  if (!back) return false;

  G4StepPoint*      before    = step  ->GetPreStepPoint();
  G4TouchableHandle touch     = before->GetTouchableHandle();
  G4int             copy      = touch ->GetCopyNumber();
  G4Track*          track     = step  ->GetTrack();
  G4double          kine      = track ->GetKineticEnergy();
  // G4int             pdg       = track ->GetDynamicParticle()->GetPDGcode();
  // G4int             trackNo   = track ->GetTrackID();
  // G4ThreeVector     position  = before->GetPosition();

  // G4cout << trackNo << " particle " << pdg << " is optical photon "
  //        << position << std::endl;

  RootIO::Instance()->AddFocalHPhoton(copy, kine);

  return true;
}
//
// EOF
//

