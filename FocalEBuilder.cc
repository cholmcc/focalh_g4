//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    FocalEBuilder.cc
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Focal-E Geometry builder 
 */
#include "FocalEBuilder.hh"
#include <G4AssemblyVolume.hh>
#include <G4Material.hh>
#include <G4NistManager.hh>
#include <G4Box.hh>
#include <G4Tubs.hh>
#include <G4LogicalVolume.hh>
#include <G4PVPlacement.hh>
#include <G4VPhysicalVolume.hh>
#include <G4ThreeVector.hh>
#include <G4RotationMatrix.hh>
#include <G4Transform3D.hh>
#include <G4NistManager.hh>
#include <G4VisAttributes.hh>
#include <G4Element.hh>
#include <G4Material.hh>
#include <G4SDManager.hh>
#include <G4VSensitiveDetector.hh>
#include "RootIO.hh"
#include "FocalE.C"
#include "FocalEHitter.hh"
#include <cassert>

// ___________________________________________________________________
struct Imprint
{
  // av_WWW_impr_XXX_YYY_pv_ZZZ
  // where the fields mean:
  // WWW - assembly volume instance number
  // XXX - assembly volume imprint number
  // YYY - the name of a log. volume we want to make a placement of
  // ZZZ - the log. volume index inside the assembly volume
  unsigned short assemblyNo;
  unsigned short imprintNo;
  std::string    volumeName;
  unsigned short volumeNo;

  Imprint(const std::string& imprintName)
  {
    std::stringstream        str(imprintName);
    std::vector<std::string> parts;
    std::string              token;
    while (std::getline(str,token,'_')) {
      parts.push_back(token);
    }

    assert(parts.size() == 7);
    assert(parts[0] == "av");
    assert(parts[2] == "impr");
    assert(parts[5] == "pv");

    assemblyNo = std::stoi(parts[1]);
    imprintNo  = std::stoi(parts[3]);
    volumeName = parts[4];
    volumeNo   = std::stoi(parts[6]);
  }
};
  

// ===================================================================
void
FocalEBuilder::CreateMaterials(G4NistManager* nistm)
{
  G4Element* elH  = nistm->FindOrBuildElement(1);
  G4Element* elC  = nistm->FindOrBuildElement(6);
  G4Element* elN  = nistm->FindOrBuildElement(7);
  G4Element* elO  = nistm->FindOrBuildElement(8);
  G4Element* elW  = nistm->FindOrBuildElement(74);//density: 19.3  I:727
  G4Element* elCu = nistm->FindOrBuildElement(29);//G4_Cu  8.96   I:322
  
  // The Kapton film (flex)
  auto matKap = new G4Material("Kapton", 1.42*g / cm3, 4);
  matKap->AddElement(elH, 2.6362 * perCent);
  matKap->AddElement(elC,69.1133 * perCent);
  matKap->AddElement(elN, 7.3270 * perCent);
  matKap->AddElement(elO,20.9235 * perCent);
  
  // The W alloy
  auto matTng = new G4Material("Tungsten", 18.73 * g / cm3, 3);
  matTng->AddElement(elW, 94 * perCent);    //the percentage of materialal
  matTng->AddElement(elN,  4 * perCent);
  matTng->AddElement(elCu, 2 * perCent);

  // The Epoxy Glue
  auto matPet = new G4Material("PET", 1.38*g / cm3, 3);
  matPet->AddElement(elC,10);
  matPet->AddElement(elH, 8);
  matPet->AddElement(elO, 4);
}
// ___________________________________________________________________
G4LogicalVolume*
FocalEBuilder::Construct(G4NistManager* nistm)
{
  auto matAir = nistm->FindOrBuildMaterial("G4_AIR");
  auto shape  = new G4Box("FocalE",
			  fParameters.GetWidth() / 2 * cm,
			  fParameters.GetHeight()/ 2 * cm,
			  fParameters.GetLength()/ 2 * cm);
  auto vol    = new G4LogicalVolume(shape,matAir,"FocalE",0,0,0);
  vol->SetVisAttributes(new G4VisAttributes(false,G4Color::Cyan()));
  auto volPad = ConstructPads(nistm);
  auto volPxl = ConstructPixels(nistm);

  std::vector<unsigned short> mapPad;
  std::vector<unsigned short> mapPxl;
  unsigned short layerNo = 0;
  G4ThreeVector  pos(0,0,-fParameters.GetLength()/2 * cm);
  for (auto t : fParameters.fLayers) {
    if (t) {
      volPxl->MakeImprint(vol, pos, 0, false, true);
      pos += G4ThreeVector(0,0,
			   fParameters.GetPixelLayerDepth() * cm +
			   fParameters.fPixelGap            * cm);
      mapPxl.push_back(layerNo);
    }
    else {
      volPad->MakeImprint(vol, pos, 0, false, true);
      pos += G4ThreeVector(0,0,
			   fParameters.GetPadLayerDepth() * cm +
			   fParameters.fPadGap            * cm);
      mapPad.push_back(layerNo);
    }
    layerNo++;
  }

  // Assign unique copy numbers to all chip and pad physical volumes.
  // The copy number encodes the position in the setup.
  // 
  // - bits 0 to 7 column number
  // - bits 8 to 15 row number
  // - bits 16 to 23 layer number
  // - bit  24 1 for back, 0 for front (pixel only)
  //   bit  25 1 for pixel 0 for pad
  //
  unsigned short nChipCol = fParameters.fNChipCol;
  unsigned short nChipRow = fParameters.fNChipRow;
  unsigned int   nChip    = nChipCol * nChipRow;
  auto           pxlIter  = volPxl->GetVolumesIterator();
  unsigned short pxlNo    = 0;
  for (size_t i=0; i< volPxl->TotalImprintedVolumes(); pxlIter++,i++)  {
    G4VPhysicalVolume* pv = *pxlIter;
    if (!pv) continue;

    G4LogicalVolume* lv = pv->GetLogicalVolume();
    if (lv != fPxlVolume) continue;

    Imprint im(pv->GetName());
    size_t         layer = mapPxl[im.imprintNo-1];
    // Pixels are placed in pairs - front and back, and starting from
    // the bottom to the top.
    bool           back  = pxlNo % 2 == 1;
    unsigned short col   = (pxlNo / 2) % nChipCol;
    unsigned short row   = ((pxlNo / 2) % nChip) / nChipCol;
    pxlNo++;
    unsigned int copy    = FocalE::CoordToCopy(true,layer,col,2*row+back); 
    pv->SetCopyNo(copy);
  }
  // Now pads 
  unsigned short nPadCol  = fParameters.fNPadCol;
  unsigned short nPadRow  = fParameters.fNPadRow;
  unsigned int   nPad     = nPadCol * nPadRow;
  auto           padIter  = volPad->GetVolumesIterator();
  unsigned short padNo    = 0;
  for (size_t i=0; i< volPad->TotalImprintedVolumes(); padIter++,i++)  {
    G4VPhysicalVolume* pv = *padIter;
    if (!pv) continue;

    G4LogicalVolume* lv = pv->GetLogicalVolume();
    if (lv != fPadVolume) continue;

    Imprint im(pv->GetName());
    size_t         layer = mapPad[im.imprintNo-1];
    // Pads are placed from the bottom up
    unsigned short col   = padNo % nPadCol;
    unsigned short row   = (padNo % nPad) / nPadCol;
    padNo++;
    unsigned int copy    = FocalE::CoordToCopy(false,layer,col,row); 
    pv->SetCopyNo(copy);
  }


  fRegion     = new G4Region("FocalE");
  fRegion->AddRootLogicalVolume(vol);

  RootIO::Instance()->Write(&fParameters,
			    "FocalEParameters");
    
  return vol;
}
// ___________________________________________________________________
G4LogicalVolume*
FocalEBuilder::ConstructAbsorber(double         depth,//In mm
				 const char*    name,
				 G4NistManager* nistm)
{
  auto matTng = nistm->FindOrBuildMaterial("Tungsten");

  double absW = fParameters.fNPadCol * fParameters.fPadW / 2 * cm;
  double absH = fParameters.fNPadRow * fParameters.fPadH / 2 * cm;
  double absZ = depth / 2;

  auto shp    = new G4Box("Absorber", absW,absH,absZ);
  auto vol    = new G4LogicalVolume(shp, matTng, name, 0,0,0);

  auto att    = new G4VisAttributes(G4Color::Grey());
  vol->SetVisAttributes(att);

  return vol;
}

// ___________________________________________________________________
G4AssemblyVolume*
FocalEBuilder::ConstructPads(G4NistManager* nistm)
{
  // This is an absorber plate with pad chips on them. Top view:
  //
  //      +----------------------------------------+
  //      | Absorber                               |
  //      +----------------------------------------+
  //      --- Glue ---------------------------------
  //      --- pads ---------------------------------
  //      --- Glue ---------------------------------
  //      --- PCB ----------------------------------
  //
  //
  // Pads are placed in a grid: Front view 
  //
  //   +-----+-----+-----+-----+-----+-----+-----+-----+-----+
  //   | Pad | Pad | Pad | Pad | Pad | Pad | Pad | Pad | Pad |
  //   +-----+-----+-----+-----+-----+-----+-----+-----+-----+
  //   | Pad | Pad | Pad | Pad | Pad | Pad | Pad | Pad | Pad |
  //   +-----+-----+-----+-----+-----+-----+-----+-----+-----+
  //   | Pad | Pad | Pad | Pad | Pad | Pad | Pad | Pad | Pad |
  //   +-----+-----+-----+-----+-----+-----+-----+-----+-----+
  //   | Pad | Pad | Pad | Pad | Pad | Pad | Pad | Pad | Pad |
  //   +-----+-----+-----+-----+-----+-----+-----+-----+-----+
  //   | Pad | Pad | Pad | Pad | Pad | Pad | Pad | Pad | Pad |
  //   +-----+-----+-----+-----+-----+-----+-----+-----+-----+
  //   | Pad | Pad | Pad | Pad | Pad | Pad | Pad | Pad | Pad |
  //   +-----+-----+-----+-----+-----+-----+-----+-----+-----+
  //   | Pad | Pad | Pad | Pad | Pad | Pad | Pad | Pad | Pad |
  //   +-----+-----+-----+-----+-----+-----+-----+-----+-----+
  //   | Pad | Pad | Pad | Pad | Pad | Pad | Pad | Pad | Pad |
  //   +-----+-----+-----+-----+-----+-----+-----+-----+-----+
  //
  auto matSi  = nistm->FindOrBuildMaterial("G4_Si");
  auto matPet = nistm->FindOrBuildMaterial("PET");

  double w    = fParameters.GetPadLayerWidth() * cm;
  double h    = fParameters.GetPadLayerHeight() * cm;
  double lyrW = w/2;
  double lyrH = h/2;
  double padW = fParameters.fPadW / 2 * cm;
  double padH = fParameters.fPadH / 2 * cm;
  
  double zGl1 = fParameters.fGlue1D    / 2 * cm;
  double zSi  = fParameters.fPadD      / 2 * cm;
  double zGl2 = fParameters.fGlue2D    / 2 * cm;
  double zFpc = fParameters.fFpcD      / 2 * cm;
  double zAbs = fParameters.fAbsorberD / 2 * cm;
  
  auto shpGl1 = new G4Box("PadGlue1", lyrW, lyrH, zGl1);
  auto shpSi  = new G4Box("PadSi",    padW, padH, zSi);
  auto shpGl2 = new G4Box("PadGlue2", lyrW, lyrH, zGl2);
  auto shpFpc = new G4Box("PadFpc",   lyrW, lyrH, zFpc);
  auto volGl1 = new G4LogicalVolume(shpGl1,matPet,"PadGlue1",0,0,0);
  auto volSi  = new G4LogicalVolume(shpSi, matSi, "PadSi",   0,0,0);
  auto volGl2 = new G4LogicalVolume(shpGl2,matPet,"PadGlue2",0,0,0);
  auto volFpc = new G4LogicalVolume(shpFpc,matPet,"PadFpc",  0,0,0);
  auto volAbs = ConstructAbsorber(2*zAbs,"PadAbsorber",nistm);
  fPadVolume  = volSi;

  auto padVis = new G4VisAttributes(G4Colour(1., 1., 0.));
  volSi->SetVisAttributes(padVis);

  // This places the absorber front at Z=0 in parent 
  G4ThreeVector posAbs(0,0,zAbs);
  G4ThreeVector posGl1 = posAbs + G4ThreeVector(0,0,zAbs+zGl1);
  G4ThreeVector posSi  = posGl1 + G4ThreeVector(0,0,zGl1+zSi);
  G4ThreeVector posGl2 = posSi  + G4ThreeVector(0,0,zSi +zGl2);
  G4ThreeVector posFpc = posGl2 + G4ThreeVector(0,0,zGl2+zFpc);
  
  auto volAss = new G4AssemblyVolume();
  volAss->AddPlacedVolume(volAbs, posAbs, 0);
  volAss->AddPlacedVolume(volGl1, posGl1, 0);
  volAss->AddPlacedVolume(volGl2, posGl2, 0);
  volAss->AddPlacedVolume(volFpc, posFpc, 0);

  // Now place the pads.  Note, these are made inside an assembly,
  // which means we cannot give them copy numbers at this point.
  for (size_t row = 0; row < fParameters.fNPadRow; row++) {
    for (size_t col = 0; col < fParameters.fNPadCol; col++) {
      auto pos = posSi + G4ThreeVector(-lyrW + padW * (2 * col +1),
				       -lyrH + padH * (2 * row +1),
				       0);
      volAss->AddPlacedVolume(volSi, pos, 0);//cpy?
    }
  }
  
  return volAss;
}
// ___________________________________________________________________
G4AssemblyVolume*
FocalEBuilder::ConstructPixels(G4NistManager* nistm)
{
  // This is two aluminium plates with Si pixel chips on them. Top view:
  //
  //      +----------------------------------------+
  //      | Absorber                               |
  //   +----------------------------------------------+
  //   | Aluminium plate front                        |
  //   +----------------------------------------------+
  //   --- chips --------------------------------------
  //
  //   --- chips --------------------------------------
  //   +----------------------------------------------+
  //   | Aluminium plate back                         |
  //   +----------------------------------------------+
  //
  //
  // The Chips are has a kapton band next to them. Front view:
  //
  //   +--------------------------------------------------+
  //   |             Kapton                               |
  //   +----------------+----------------+----------------+
  //   |  Chip          |  Chip          |  Chip          |
  //   +----------------+----------------+----------------+
  //   |             Kapton                               |
  //   +----------------+----------------+----------------+
  //   |  Chip          |  Chip          |  Chip          |
  //   +----------------+----------------+----------------+
  //   |             Kapton                               |
  //   +----------------+----------------+----------------+
  //   |  Chip          |  Chip          |  Chip          |
  //   +----------------+----------------+----------------+
  //
  // On the back plate the kapton band goes below the chips 
  // 
  auto matSi  = nistm->FindOrBuildMaterial("G4_Si");
  auto matAl  = nistm->FindOrBuildMaterial("G4_Al");
  auto matKap = nistm->FindOrBuildMaterial("Kapton");

  double alW     = fParameters.GetPixelLayerWidth()   / 2 * cm;
  double alH     = fParameters.GetPixelLayerHeight()  / 2 * cm;
  double alZ     = fParameters.fAlD                   / 2 * cm;
  double flxW    = alW;
  double flxH    = fParameters.fFlexH                 / 2 * cm;
  double flxZ    = fParameters.fPixelD                / 2 * cm;
  double alpW    = fParameters.GetChipWidth()         / 2 * cm;
  double alpH    = fParameters.GetChipHeight()        / 2 * cm;
  double alpFlxH = fParameters.GetChipAndFlexHeight()	  * cm;
  double alpZ    = fParameters.fPixelD                / 2 * cm;
  double absZ    = fParameters.fAbsorberD             / 2 * cm;
  double alGap   = fParameters.fAlGap                     * cm;
  double chipO   = fParameters.fChipO                     * cm;
  
  auto shpFlx = new G4Box("PxlFlex",   flxW,   flxH,   flxZ);
  auto shpAlp = new G4Box("PxlAlpide", alpW,   alpH,   alpZ);
  auto shpAl  = new G4Box("PxlAl",     alW,    alH,    alZ);
  auto volFlx = new G4LogicalVolume(shpFlx,matKap,"PxlFlex",  0,0,0);
  auto volAlp = new G4LogicalVolume(shpAlp,matSi, "PxlAlpide",0,0,0);
  auto volAl  = new G4LogicalVolume(shpAl, matAl, "PxlAl",    0,0,0);
  auto volAbs = ConstructAbsorber(2 * absZ, "PxlAbsorber",nistm);
  fPxlVolume  = volAlp;

  auto alpVis = new G4VisAttributes(G4Colour(1., 1., 0.));
  auto flxVis = new G4VisAttributes(G4Colour(1., 0,  0.));
  auto alVis  = new G4VisAttributes(G4Colour(0., 0,  1.));
  volAlp->SetVisAttributes(alpVis);
  volFlx->SetVisAttributes(flxVis);
  volAl ->SetVisAttributes(alVis);
  
  // This places the front face of the absorber at Z=0 in parent 
  G4ThreeVector posAbs(0,0,absZ);
  G4ThreeVector posAl1 = posAbs + G4ThreeVector(0,0,absZ   + alZ);
  G4ThreeVector posAl2 = posAl1 + G4ThreeVector(0,0,alGap  + alZ);
  G4ThreeVector posBs1 = posAl1 + G4ThreeVector(0,0,alZ    + alpZ);
  G4ThreeVector posBs2 = posAl2 + G4ThreeVector(0,0,-alZ   - alpZ);
  
  auto volAss = new G4AssemblyVolume();
  volAss->AddPlacedVolume(volAbs,posAbs, 0);
  volAss->AddPlacedVolume(volAl, posAl1, 0);
  volAss->AddPlacedVolume(volAl, posAl2, 0);

  for (size_t row = 0; row < fParameters.fNChipRow; row++) {
    // Place kapton cable 
    // Front placement
    // - bottom
    // - plus flex half height
    // - plus row times flex and chip full height
    // - plus chip full height
    auto posFlxF =
      posBs1 + G4ThreeVector(0,
			     -alH + flxH + row * alpFlxH + 2*alpH,
			     0);
    // Back placement
    // - bottom
    // - plus flex half-height
    // - plus row times flex and chip full height
    // - plus offset 
    auto posFlxB =
      posBs2 + G4ThreeVector(0,
			     -alH + flxH + row * alpFlxH + chipO,
			     0);
    volAss->AddPlacedVolume(volFlx, posFlxF, 0); // Copy? 
    volAss->AddPlacedVolume(volFlx, posFlxB, 0); // Copy? 
    for (size_t col = 0; col < fParameters.fNChipCol; col++) {
      // Horizontal
      // - Left
      // - plus chip half width times
      // - plus column times chip full width
      double x = -alW + alpW * (2 * col + 1);
      // Front placement
      // Vertical (below flex cable)
      // - Flex position
      // - minus chip half height
      // - minus flex half height
      auto posF = posFlxF + G4ThreeVector(x, -alpH - flxH, 0);
      // Back placement 
      // Vertical 
      // - Flex position
      // - plus chip half height
      // - plus flex half height 
      auto posB = posFlxB + G4ThreeVector(x, +alpH + flxH, 0);
      volAss->AddPlacedVolume(volAlp, posF, 0); // Copy? 
      volAss->AddPlacedVolume(volAlp, posB, 0); // Copy? 
    }
  }
  
  return volAss;
}
// ___________________________________________________________________
std::map<G4VSensitiveDetector*,G4LogicalVolume*>
FocalEBuilder::CreateSensitive()
{
  return {{new FocalEHitter(), fPadVolume},
	  {new FocalEHitter(), fPxlVolume} };
}


// ___________________________________________________________________
//
// EOF
//
 
