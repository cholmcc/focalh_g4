//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    OnInit.hh
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Initialise user actions
 */
#ifndef __OnInit__
#define __OnInit__
#include <G4VUserActionInitialization.hh>
#include "Generator.hh"
#include "OnStack.hh"
#include "OnRun.hh"
#include "OnEvent.hh"
#include "Builder.hh"
// #include "OnStep.hh"

class OnInit : public G4VUserActionInitialization
{
public:
  OnInit(const Builder* geom)
    : G4VUserActionInitialization(),
      fGeometry(geom)
  {}
  void Build() const
  {
    SetUserAction(new Generator(-fGeometry->GetCaveLength()/2));
    SetUserAction(new OnStack());
    SetUserAction(new OnEvent());
    SetUserAction(new OnRun());
  }
protected:
  const Builder* fGeometry;
};
#endif
//
// EOF
// 
  
    
    
