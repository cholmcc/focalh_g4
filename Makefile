#____________________________________________________________________ 
#  
#  FoCAL-H Geant 4 Simulation
#  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public License
#  as published by the Free Software Foundation; either version 2.1
#  of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free
#  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
#  02111-1307 USA
#
MODULE		:= FocalH
MODULE_MAP	:= $(MODULE).rootmap
ROOT_CLING	:= rootcling
ROOT_CPPFLAGS	:= $(filter -I%, $(shell root-config --cflags)) \
		   $(filter -D%, $(shell root-config --cflags))
ROOT_CXXFLAGS	:= $(filter-out -D%, \
		     $(filter-out -I%, $(shell root-config --cflags)))
ROOT_LDFLAGS	:= $(shell root-config --ldflags) \
		   $(filter-out -l%, $(shell root-config --libs))
ROOT_LIBS	:= $(filter -l%, $(shell root-config --libs)) -lEG
GEANT4_CPPFLAGS	= $(filter -I%, $(shell geant4-config --cflags)) \
		   $(filter -D%, $(shell geant4-config --cflags)) 
GEANT4_CXXFLAGS	:= $(filter-out -D%, \
		     $(filter-out -I%, $(shell geant4-config --cflags))) 
GEANT4_LDFLAGS	:= $(filter-out -l%, $(shell geant4-config --libs))
GEANT4_LIBS	:= $(filter -l%, $(shell geant4-config --libs)) 

WITH_QT3D	:= 1
WITH_GDML	:= 1
ifdef WITH_QT3D
GEANT4_CPPFLAGS	+= -DG4VIS_USE_QT3D
GEANT4_LIBS	+= -lG4visQt3D
endif
ifdef WITH_GDML
GEANT4_LIBS	+= -lxerces-c
endif

srcdir		:=
include Files.mk

CXX		:= g++ -c
CXXFLAGS	:= $(GEANT4_CPPFLAGS) $(ROOT_CPPFLAGS) -Wno-shadow -g
CPPFLAGS	:= $(GEANT4_CXXFLAGS) $(ROOT_CXXFLAGS)
LD		:= g++
LDFLAGS		:= $(ROOT_LDFLAGS) $(GEANT4_LDFLAGS)
LIBS		:= $(ROOT_LIBS) $(GEANT4_LIBS)

SOURCES		:= $(SIM_SOURCES)
HEADERS		:= $(SIM_HEADERS)
OBJECTS		:= $(SOURCES:%.cc=%.o)

%.o:%.cc
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $<

%:%.o
	$(LD) $(LDFLAGS) $^ $(LIBS) -o $@



all:		main

clean:
	rm -f *~ *.o *_C* main *.log Dict.cc *.pcm *.rootmap *.so

realclean:clean
	rm -f *.root

Dict.cc:	$(DATA_SOURCES) EventTree.C
	$(ROOT_CLING) -f $@ \
		-s $(MODULE).so \
		-rml $(MODULE).so \
		-rmf $(MODULE_MAP) \
		-DCLING $^

main:		$(OBJECTS) Dict.o

main.o: 	$(srcdir)main.cc		\
		$(srcdir)Builder.hh 		\
		$(srcdir)FocalH.C 		\
		$(srcdir)FocalE.C 		\
		$(srcdir)FocalHBuilder.hh 	\
		$(srcdir)FocalEBuilder.hh 	\
		$(srcdir)OnInit.hh 		\
		$(srcdir)Generator.hh 		\
		$(srcdir)OnStack.hh 		\
		$(srcdir)OnRun.hh 		\
		$(srcdir)OnEvent.hh 		\
		$(srcdir)RootIO.hh 		\
		$(srcdir)EventTree.C 		\
		$(srcdir)FocalHHit.C 		\
		$(srcdir)FocalEHit.C 		\
		$(srcdir)FocalHSum.C 		\
		$(srcdir)FocalHFlux.C

Builder.o:	$(srcdir)Builder.cc		\
		$(srcdir)Builder.hh		\
		$(srcdir)FocalE.C		\
		$(srcdir)FocalH.C		\
		$(srcdir)Fast.hh		\
		$(srcdir)FocalHBuilder.hh	\
		$(srcdir)FocalEBuilder.hh	\
		$(srcdir)RootIO.hh		\
		$(srcdir)EventTree.C		\
		$(srcdir)FocalHHit.C		\
		$(srcdir)FocalEHit.C		\
		$(srcdir)FocalHSum.C		\
		$(srcdir)FocalHFlux.C		\
		$(srcdir)AirOptical.C

 # --- Dict.cc 
Dict.o:		$(srcdir)Dict.cc		\
		$(srcdir)FocalH.C		\
		$(srcdir)FocalHHit.C		\
		$(srcdir)FocalHSum.C		\
		$(srcdir)FocalHFlux.C		\
		$(srcdir)FocalE.C		\
		$(srcdir)FocalEHit.C		\
		$(srcdir)EventTree.C		

Fast.o:		$(srcdir)Fast.cc		\
		$(srcdir)Fast.hh		

FocalEBuilder.o:$(srcdir)FocalEBuilder.cc	\
		$(srcdir)FocalEBuilder.hh	\
		$(srcdir)RootIO.hh		\
		$(srcdir)EventTree.C		\
		$(srcdir)FocalHHit.C		\
		$(srcdir)FocalH.C		\
		$(srcdir)FocalEHit.C		\
		$(srcdir)FocalE.C		\
		$(srcdir)FocalHSum.C		\
		$(srcdir)FocalHFlux.C		\
		$(srcdir)FocalEHitter.hh

FocalEHitter.o:	$(srcdir)FocalEHitter.cc	\
		$(srcdir)FocalEHitter.hh	\
		$(srcdir)RootIO.hh		\
		$(srcdir)EventTree.C		\
		$(srcdir)FocalHHit.C		\
		$(srcdir)FocalH.C		\
		$(srcdir)FocalEHit.C		\
		$(srcdir)FocalE.C		\
		$(srcdir)FocalHSum.C		\
		$(srcdir)FocalHFlux.C	

FocalHBuilder.o:$(srcdir)FocalHBuilder.cc	\
		$(srcdir)FocalHBuilder.hh	\
		$(srcdir)FocalH.C		\
		$(srcdir)Fast.hh		\
		$(srcdir)FocalHHitter.hh	\
		$(srcdir)FocalHSummer.hh	\
		$(srcdir)FrontBack.hh		\
		$(srcdir)FocalHFluxer.hh	\
		$(srcdir)ScintOptical.C		\
		$(srcdir)RootIO.hh		\
		$(srcdir)EventTree.C		\
		$(srcdir)FocalHHit.C		\
		$(srcdir)FocalEHit.C		\
		$(srcdir)FocalE.C		\
		$(srcdir)FocalHSum.C		\
		$(srcdir)FocalHFlux.C

FocalHFluxer.o:	$(srcdir)FocalHFluxer.cc	\
		$(srcdir)FocalHFluxer.hh	\
		$(srcdir)FrontBack.hh		\
		$(srcdir)RootIO.hh		\
		$(srcdir)EventTree.C		\
		$(srcdir)FocalHHit.C		\
		$(srcdir)FocalH.C		\
		$(srcdir)FocalEHit.C		\
		$(srcdir)FocalE.C		\
		$(srcdir)FocalHSum.C		\
		$(srcdir)FocalHFlux.C		

FocalHHitter.o:	$(srcdir)FocalHHitter.cc	\
		$(srcdir)FocalHHitter.hh	\
		$(srcdir)RootIO.hh		\
		$(srcdir)EventTree.C		\
		$(srcdir)FocalHHit.C		\
		$(srcdir)FocalH.C		\
		$(srcdir)FocalEHit.C		\
		$(srcdir)FocalE.C		\
		$(srcdir)FocalHSum.C		\
		$(srcdir)FocalHFlux.C		

FocalHSummer.o:	$(srcdir)FocalHSummer.cc	\
		$(srcdir)FocalHSummer.hh	\
		$(srcdir)FrontBack.hh		\
		$(srcdir)RootIO.hh		\
		$(srcdir)EventTree.C		\
		$(srcdir)FocalHHit.C		\
		$(srcdir)FocalH.C		\
		$(srcdir)FocalEHit.C		\
		$(srcdir)FocalE.C		\
		$(srcdir)FocalHSum.C		\
		$(srcdir)FocalHFlux.C		

FrontBack.o:	$(srcdir)FrontBack.cc		\
		$(srcdir)FrontBack.hh		

Generator.o:	$(srcdir)Generator.cc		\
		$(srcdir)Generator.hh		

OnEvent.o:	$(srcdir)OnEvent.cc		\
		$(srcdir)OnEvent.hh		\
		$(srcdir)RootIO.hh		\
		$(srcdir)EventTree.C		\
		$(srcdir)FocalHHit.C		\
		$(srcdir)FocalH.C		\
		$(srcdir)FocalEHit.C		\
		$(srcdir)FocalE.C		\
		$(srcdir)FocalHSum.C		\
		$(srcdir)FocalHFlux.C		

OnStack.o:	$(srcdir)OnStack.cc		\
		$(srcdir)OnStack.hh		\
		$(srcdir)RootIO.hh		\
		$(srcdir)EventTree.C		\
		$(srcdir)FocalHHit.C		\
		$(srcdir)FocalH.C		\
		$(srcdir)FocalEHit.C		\
		$(srcdir)FocalE.C		\
		$(srcdir)FocalHSum.C		\
		$(srcdir)FocalHFlux.C		

RootIO.o:	$(srcdir)RootIO.cc		\
		$(srcdir)RootIO.hh		\
		$(srcdir)EventTree.C		\
		$(srcdir)FocalHHit.C		\
		$(srcdir)FocalH.C		\
		$(srcdir)FocalEHit.C		\
		$(srcdir)FocalE.C		\
		$(srcdir)FocalHSum.C		\
		$(srcdir)FocalHFlux.C	

#
# EOF
#
