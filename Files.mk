#
# The files
#
DATA_SOURCES	:= $(srcdir)Hit.C			\
		   $(srcdir)FocalH.C			\
		   $(srcdir)FocalE.C			\
		   $(srcdir)FocalHHit.C			\
		   $(srcdir)FocalEHit.C			\
		   $(srcdir)FocalHDigit.C		\
		   $(srcdir)FocalHFlux.C		\
		   $(srcdir)FocalHSum.C			\
		   $(srcdir)EventTree.C			

ANA_SOURCES	:= $(srcdir)Analyse.C			\
		   $(srcdir)Analyser.C			\
		   $(srcdir)Profiler.C			\
		   $(srcdir)Leaving.C			\
		   $(srcdir)FocalHDigitizer.C

SIM_SOURCES	:= $(srcdir)main.cc			\
		   $(srcdir)RootIO.cc			\
		   $(srcdir)FocalEBuilder.cc		\
		   $(srcdir)FocalHBuilder.cc		\
		   $(srcdir)Builder.cc			\
		   $(srcdir)Generator.cc		\
		   $(srcdir)OnStack.cc			\
		   $(srcdir)OnEvent.cc			\
		   $(srcdir)Fast.cc			\
		   $(srcdir)FrontBack.cc		\
		   $(srcdir)FocalHHitter.cc		\
		   $(srcdir)FocalEHitter.cc		\
		   $(srcdir)FocalHSummer.cc		\
		   $(srcdir)FocalHFluxer.cc

SIM_HEADERS	:= $(srcdir)FocalEBuilder.hh		\
		   $(srcdir)FocalHBuilder.hh		\
		   $(srcdir)Builder.hh			\
		   $(srcdir)Generator.hh		\
		   $(srcdir)OnInit.hh			\
		   $(srcdir)OnRun.hh			\
		   $(srcdir)OnStack.hh			\
		   $(srcdir)OnEvent.hh			\
		   $(srcdir)Fast.hh			\
		   $(srcdir)FrontBack.hh		\
		   $(srcdir)FocalHHitter.hh		\
		   $(srcdir)FocalEHitter.hh		\
		   $(srcdir)FocalHSummer.hh		\
		   $(srcdir)FocalHFluxer.hh		\
		   $(srcdir)RootIO.hh			\
		   $(DATA_SOURCES)




#
#  EOF
#
