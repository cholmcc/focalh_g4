//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    FocalHBuilder.cc
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Focal-H Geometry builder 
 */
#include "FocalHBuilder.hh"
#include "Fast.hh"
#include <G4AssemblyVolume.hh>
#include <G4Material.hh>
#include <G4NistManager.hh>
#include <G4Box.hh>
#include <G4Tubs.hh>
#include <G4LogicalVolume.hh>
#include <G4PVPlacement.hh>
#include <G4VPhysicalVolume.hh>
#include <G4ThreeVector.hh>
#include <G4RotationMatrix.hh>
#include <G4Transform3D.hh>
#include <G4NistManager.hh>
#include <G4VisAttributes.hh>
#include <G4Element.hh>
#include <G4Material.hh>
#include <G4SDManager.hh>
#include <G4VSensitiveDetector.hh>
#include "FocalH.C"
#include "FocalHHitter.hh"
#include "FocalHSummer.hh"
#include "FocalHFluxer.hh"
#include "ScintOptical.C"
#include "RootIO.hh"
#include <cassert>

// ===================================================================
void
FocalHBuilder::CreateMaterials(G4NistManager* nistm)
{
  G4Element* elH = nistm->FindOrBuildElement(1);
  G4Element* elC = nistm->FindOrBuildElement(6);
  nistm->FindOrBuildMaterial("G4_Cu");
 
  // The scintillator properties
  G4MaterialPropertiesTable* tab  = new G4MaterialPropertiesTable();
  tab->AddConstProperty("SCINTILLATIONTIMECONSTANT1",
			ScintOptical::kTimeConstant*ns);
  tab->AddConstProperty("SCINTILLATIONYIELD",
			ScintOptical::kYield/MeV);
  tab->AddConstProperty("RESOLUTIONSCALE",
			ScintOptical::kResolution);
  tab->AddProperty("SCINTILLATIONCOMPONENT1",
		   ScintOptical::kEnergies,
		   ScintOptical::kScintillation,
		   ScintOptical::kNPoints);
  tab->AddProperty("RINDEX",
		   ScintOptical::kEnergies,
		   ScintOptical::kRefractionIndex,
		   ScintOptical::kNPoints);
  tab->AddProperty("ABSLENGTH",
		   ScintOptical::kEnergies,
		   ScintOptical::kAbsorbtionLength,
		   ScintOptical::kNPoints);

  // POLYSTYRENE
  G4double    density  = 1.05 * g / cm3;
  G4Material* matScint = new G4Material("G4_POLYSTYRENE", density, 2);
  matScint->AddElement(elC, 8);
  matScint->AddElement(elH, 8);
  matScint->GetIonisation()->SetBirksConstant(0.126*mm/MeV);
  matScint->SetMaterialPropertiesTable(tab);
}

// ___________________________________________________________________
G4LogicalVolume*
FocalHBuilder::Construct(G4NistManager* nistm)
{
  auto   matAir = nistm->FindOrBuildMaterial("G4_AIR");
  auto   matAbs = nistm->FindOrBuildMaterial("G4_Cu");
  auto   matScn = nistm->FindOrBuildMaterial("G4_POLYSTYRENE");
  
  // double w      = fParameters.GetWidth() * cm;
  // double h      = fParameters.GetHeight() * cm;
  double w, h;
  fParameters.ContainerSize(w,h);
  w             *= cm;
  h             *= cm;
  double eps    = fParameters.fEpsilon * cm;
  double flxL   = fParameters.GetFluxLength() / 2 * cm;
  double flxW   = (w + eps) / 2;
  double flxH   = (h + eps) / 2;
  double boxTX  = fParameters.fBoxThick * cm - .1;
  double boxTW  = flxW + boxTX;
  double boxTH  = boxTX / 2;
  double boxTY  = flxH + boxTH;
  double boxL   = flxL;
  double boxSW  = boxTH;
  double boxSH  = flxH;
  double boxSX  = flxW + boxSW;
  double modW   = flxW + boxTX;
  double modH   = flxH + boxTX;
  double modL   = boxL;
  double mthW   = GetWidth()/2 * 1.01; // Some wickle room
  double mthH   = GetHeight()/2 * 1.01; // Some wickle room
  double mthL   = modL;
  double rT     = fParameters.fTubeRadius  * cm;
  double r0     = fParameters.fScintRadius * cm;
  double rS     = r0 - eps;
  double l      = fParameters.fLength / 2  * cm;

  std::cout << "Flux   volume " << flxW << "\t" << flxH << "\t" << flxL << "\n"
	    << "Module volume " << modW << "\t" << modH << "\t" << modL << "\n"
	    << "Mother volume " << mthW << "\t" << mthH << "\t" << mthL
	    << std::endl;
  auto shpAbs = new G4Tubs("Absorber",    r0, rT, l, 0.,2*M_PI*rad);
  auto shpScn = new G4Tubs("Scintilator", 0,  rS, l, 0.,2*M_PI*rad);
  auto shpFlx = new G4Box("Fluxer",flxW,flxH,flxL);
  auto shpTop = new G4Box("BoxTop", boxTW,boxTH,boxL);
  auto shpSde = new G4Box("BoxSide",boxSW,boxSH,boxL);
  auto shpMod = new G4Box("Module", modW, modH, modL);
  auto shpMth = new G4Box("FocalH", mthW, mthH, mthL);

  // Should module be an assembly? 
  auto volAbs = new G4LogicalVolume(shpAbs,matAbs,"Absorber",   0,0,0);
  auto volScn = new G4LogicalVolume(shpScn,matScn,"Scintilator",0,0,0);
  auto volStw = new G4AssemblyVolume();
  auto volFlx = new G4LogicalVolume(shpFlx,matAir,"Fluxer",     0,0,0);
  auto volTop = new G4LogicalVolume(shpTop,matAbs,"BoxTop",     0,0,0);
  auto volSde = new G4LogicalVolume(shpSde,matAbs,"BoxSide",    0,0,0);
  auto volMod = new G4LogicalVolume(shpMod,matAir,"Module",     0,0,0);
  auto volMth = new G4LogicalVolume(shpMth,matAir,"FocalH",     0,0,0);
  
  volAbs->SetVisAttributes(new G4VisAttributes(G4Color::Brown()));
  volScn->SetVisAttributes(new G4VisAttributes(G4Color::Yellow()));
  volFlx->SetVisAttributes(new G4VisAttributes(false,G4Color::Cyan()));
  volTop->SetVisAttributes(new G4VisAttributes(G4Color::Brown()));
  volSde->SetVisAttributes(new G4VisAttributes(G4Color::Brown()));
  volMod->SetVisAttributes(new G4VisAttributes(false, G4Color::Blue()));
  volMth->SetVisAttributes(new G4VisAttributes(false, G4Color::Yellow()));
  fFluxVolume    = volFlx;
  fScintVolume   = volScn;

  // Place physical volumes in module logical volume 
  new G4PVPlacement(G4TranslateY3D( boxTY),volTop,volTop->GetName(),volMod,
		    false, 0);
  new G4PVPlacement(G4TranslateY3D(-boxTY),volTop,volTop->GetName(),volMod,
		    false, 1);
  new G4PVPlacement(G4TranslateX3D(-boxSX),volSde,volSde->GetName(),volMod,
		    false, 0);
  new G4PVPlacement(G4TranslateX3D( boxSX),volSde,volSde->GetName(),volMod,
		    false, 1);
  new G4PVPlacement(G4Transform3D(),volFlx,volFlx->GetName(),volMod, false, 0);
  
  // volMod->AddPlacedVolume(volTop, G4ThreeVector(0,      boxTY, 0), 0);
  // volMod->AddPlacedVolume(volTop, G4ThreeVector(0,     -boxTY, 0), 0);
  // volMod->AddPlacedVolume(volSde, G4ThreeVector(-boxSX, 0,     0), 0);
  // volMod->AddPlacedVolume(volSde, G4ThreeVector( boxSX, 0,     0), 0);
  // volMod->AddPlacedVolume(volFlx, G4ThreeVector(0,      0,     0), 0);
  G4ThreeVector null(0,0,0);
  volStw->AddPlacedVolume(volAbs, null, 0);
  volStw->AddPlacedVolume(volScn, null, 0);

  // Map imprint index to copy number 
  std::vector<int> mapping;
  // Make physical volumes of straws in flux volume 
  for (unsigned short r = 0; r < fParameters.fNTubeRow; r++) {
    unsigned short nc = fParameters.ColumnsInRow(r);
    for (unsigned short c = 0; c < nc; c++) {
      G4double x, y;
      fParameters.TubeXY(x,y,c,r);
      x *= cm;
      y *= cm;
      
      unsigned int copy = FocalH::CoordToCopy(0,0,c,r);
      mapping.push_back(copy);
#if 0
      std::cout << std::setw(3) << mapping.size() << " -> "
		<< std::setfill('0') << std::hex
		<< std::setw(8) << copy
		<< std::dec << std::setfill(' ') << " ["
		<< std::setw(3) << c  << "," 
		<< std::setw(3) << r << "] @ "
		<< x << "\t" << y << " ("
		<< flxW << "\t" << flxH << std::endl;
#endif
      
      G4ThreeVector pos  = G4ThreeVector(x, y, 0.);
      volStw->MakeImprint(volFlx, pos, 0);
    }
  }
  auto iter  = volStw->GetVolumesIterator();
  auto miter = mapping.begin();
  size_t cnt = 0;
  while (*iter && miter != mapping.end()) {
    G4VPhysicalVolume* vol = *iter;
    if (vol->GetLogicalVolume()->GetMaterial()->GetName() != "G4_POLYSTYRENE") {
      iter++;
      continue;
    }

#if 0
    // Below is a check 
    unsigned int copy = *miter;
    unsigned short col, row;
    FocalH::CopyToCoord(copy,col,row);

    G4ThreeVector vt = vol->GetTranslation();
    G4double x, y;
    FocalH::CoordToXY(col,row,rT,fParameters.fNCol,fParameters.fNRow,x,y);

    if (std::fabs(x - vt.x()) > 0.01 || std::fabs(y - vt.y()) > 0.01) 
      std::cout << vol->GetName() << "\t"
		<< std::setw(5) << cnt << " "
		<< std::setfill('0') << std::hex
		<< std::setw(8) << copy
		<< std::dec << std::setfill(' ') << " ["
		<< std::setw(3) << col  << "," 
		<< std::setw(3) << row << "]\t"
		<< std::setw(10) << x << ","
		<< std::setw(10) << y << "  vs  "
		<< std::setw(10) << vt.x() << ","
		<< std::setw(10) << vt.y() <<  std::endl;
#endif
    
    vol->SetCopyNo(*miter);
    miter++;
    iter++;
    cnt++;
  }
  // Place modules inside mother volume 
  for (unsigned short r = 0; r < fParameters.fNModuleRow; r++) {
    for (unsigned short c = 0; c < fParameters.fNModuleCol; c++) {
      double x, y;
      fParameters.ModuleXY(x,y,c,r);
      int    copy = FocalH::CoordToCopy(c,r,0,0);
      new G4PVPlacement(G4Translate3D(x*cm,y*cm,0),
			volMod,volMod->GetName(),volMth,
			false, copy);
      std::cout << "Placing module " << copy << "\t at " << x << "\t" << y
		<< std::endl;
    }
  }

  fRegion     = new G4Region("FocalH");
  fRegion->AddRootLogicalVolume(fFluxVolume);
  // fFocalH->AddRootLogicalVolume(fScintVolume);
  if (fParameters.fFast) new Fast(fRegion);

  RootIO::Instance()->Write(&fParameters, "FocalH");

  return volMth;
}
// ___________________________________________________________________
std::map<G4VSensitiveDetector*,G4LogicalVolume*>
FocalHBuilder::CreateSensitive()
{
  return {
    { new FocalHHitter(), fScintVolume },
    { new FocalHSummer(fScintVolume,
		       fParameters.fLength   * cm,
		       fParameters), fScintVolume },
    { new FocalHFluxer(fFluxVolume,
		       fParameters.GetFluxLength() * cm), fFluxVolume } };
}


// ___________________________________________________________________
//
// EOF
//
 
